﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.ServiceModel;
using RenderMonitor.Def.Process;
using RenderMonitor.Process;
using RenderService.Core.Def;
using RenderService.Domain;

namespace RenderMonitor {
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class RenderMonitor : IRenderMonitor, IDisposable {
        private readonly IRenderProcessFactory _factory;
        private readonly ISystemManager _systemManager;
        private readonly ConcurrentDictionary<IRenderProcess, InProgressRender> _processes;

        private IRenderJobManager _jobManager;

        public RenderMonitor()
            : this(new LuxProcessFactory()) {
        }

        public RenderMonitor(IRenderProcessFactory factory)
            : this(factory, jobManager: null, systemManager: new SystemManager()) {
        }

        public RenderMonitor(IRenderProcessFactory factory,
                             IRenderJobManager jobManager,
                             ISystemManager systemManager) {
            Contract.Requires(factory != null);
            Contract.Requires(jobManager != null);

            _factory = factory;
            _systemManager = systemManager;
            _processes = new ConcurrentDictionary<IRenderProcess, InProgressRender>();

            _jobManager = jobManager;
        }

        public void Register(IRenderJobManager jobManager) {
            Contract.Requires(jobManager != null);

            if (_jobManager != null) {
                throw new InvalidOperationException("Already registered with a job manager.");
            }

            jobManager.RegisterMonitor();

            _jobManager = jobManager;
        }

        public void StopRender(RenderJob renderJob) {
            Contract.Requires(renderJob != null);

            var process = _processes.Where(kv => kv.Value.Job.ID == renderJob.ID)
                                    .Select(kv => kv.Key)
                                    .FirstOrDefault();
            if (process != null) {
                InProgressRender render = null;
                _processes.TryRemove(process, out render);


                process.OnExecuteAsyncCompleted -= OnRenderProcessCompleted;
                process.Dispose();
            }
        }

        public void RenderAsync(RenderJob renderJob) {
            Contract.Requires(renderJob != null);

            var sceneName = Path.GetFileNameWithoutExtension(renderJob.Scene);
            var render = new InProgressRender {
                Job = renderJob,
                ProgressiveSteps = new Queue<RenderSpecification>()
            };

            int progressiveSteps = renderJob.ProgressiveSteps;
            for (int i = 1; i <= progressiveSteps; i++) {
                var xres = (int)(((double)i / (double)progressiveSteps) * renderJob.XResolution);
                var yres = (int)(((double)i / (double)progressiveSteps) * renderJob.YResolution);
                var spp = (int)(((double)i / (double)progressiveSteps) * renderJob.SamplesPerPixel);
                var spec = new RenderSpecification {
                    XResolution = Math.Max(xres, 1),
                    YResolution = Math.Max(yres, 1),
                    SamplesPerPixel = Math.Max(spp, 1),
                    OutputName = string.Format("{0}_{1}x{2}.png", sceneName, xres, yres)
                };

                if (renderJob.Results.Where(r => r.Status == RenderResultStatus.PartialResultComplete &&
                                                 r.XResolution == spec.XResolution &&
                                                 r.YResolution == spec.YResolution &&
                                                 r.SamplesPerPixel == spec.SamplesPerPixel)
                                     .Count() == 0) {
                    // The result for this progressive step has _not been computed yet.
                    render.ProgressiveSteps.Enqueue(spec);
                }
            }

            RenderAsync(render);
        }

        private void RenderAsync(InProgressRender render) {
            var spec = render.ProgressiveSteps.Dequeue();

            var process = _factory.CreateProcess(render.Job,
                                                 spec.XResolution,
                                                 spec.YResolution,
                                                 spec.SamplesPerPixel,
                                                 spec.OutputName);
            _processes.TryAdd(process, render);

            process.OnExecuteAsyncCompleted += OnRenderProcessCompleted;
            process.OnRenderResultChanged += (s, e) => OnRenderProcessUpdated(e.Result);

            Console.WriteLine("Job #{0}: rendering at Resolution: {1}*{2}\t\tS/P: {3}",
                              render.Job.ID,
                              spec.XResolution,
                              spec.YResolution,
                              spec.SamplesPerPixel);

            process.ExecuteAsync();
        }

        private void OnRenderProcessCompleted(object sender, RenderResultChangedEventArgs result) {
            var process = (IRenderProcess)sender;
            var renderResult = result.Result;

            InProgressRender render = null;
            _processes.TryRemove(process, out render);

            if (render.ProgressiveSteps.Count == 0) {
                renderResult.IsFinalResult = true;
                Console.WriteLine("Job #{0}: {1}.",
                                  renderResult.RenderJob.ID,
                                  renderResult.Status == RenderResultStatus.Complete ? "Completed" : "Faulted");
            }
            else {
                renderResult.Status = (renderResult.Status == RenderResultStatus.Complete) ?
                                        RenderResultStatus.PartialResultComplete :
                                        RenderResultStatus.PartialResultFaulted;
            }

            OnRenderProcessUpdated(renderResult);

            if (render.ProgressiveSteps.Count > 0) {
                // Continue progressively rendering
                RenderAsync(render);
            }
        }

        private void OnRenderProcessUpdated(RenderResult renderResult) {
            _jobManager.ReportRenderResult(renderResult);
        }

        public void Dispose() {
            foreach (var p in _processes.Keys) {
                p.Dispose();
            }

            if (_jobManager != null) {
                _jobManager.UnregisterMonitor();
                _jobManager = null;
            }
        }

        public RenderMonitorInfo GetInformation() {
            return new RenderMonitorInfo {
                MachineName = _systemManager.MachineName,
                MachineCpuUsage = _systemManager.MachineCpuUsage,
                MachineAvailableRAM = _systemManager.MachineAvailableRAM,
                MachineIPv4Addresss = _systemManager.MachineIPv4Address,

                // IMPORTANT: Evaluate this linq statement immediately since this
                // object has to be serialized.
                AssignedJobIds = _processes.Select(pair => pair.Value.Job.ID).ToList()
            };
        }
    }
}