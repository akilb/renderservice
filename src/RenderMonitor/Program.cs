﻿using System;
using System.ServiceModel;
using RenderService.Core.Def;
using System.Threading;

namespace RenderMonitor {
    class Program {
        static void Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine("usage: RenderMonitor.exe <address of renderservice>");
                return;
            }

            var renderServiceEndpoint = Endpoints.GetRenderJobManagerEndpoint(args[0]);
            // Give the RenderService time to start.
            Thread.Sleep(3000);

            using (var renderMonitor = new RenderMonitor()) {
                DuplexChannelFactory<IRenderJobManager> cf =
                            new DuplexChannelFactory<IRenderJobManager>(
                            renderMonitor,
                            new NetTcpBinding() {
                                SendTimeout = TimeSpan.MaxValue,
                                ReceiveTimeout = TimeSpan.MaxValue
                            },
                            renderServiceEndpoint);
                var jobManager = cf.CreateChannel();

                Register(jobManager, renderMonitor);

                Console.WriteLine("RenderMonitor is running. Press <ENTER> to exit.");
                Console.ReadLine();
            }
        }

        private static void Register(IRenderJobManager renderService, RenderMonitor monitor) {
            try {
                monitor.Register(renderService);
            }
            catch (EndpointNotFoundException) {
                Register(renderService, monitor);
            }
        }
    }
}
