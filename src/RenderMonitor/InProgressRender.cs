﻿using System.Collections.Generic;
using RenderService.Domain;

namespace RenderMonitor {
    public class InProgressRender {
        public RenderJob Job { get; set; }
        public Queue<RenderSpecification> ProgressiveSteps { get; set; }
    }

    public class RenderSpecification {
        public int XResolution      { get; set; }
        public int YResolution      { get; set; }
        public int SamplesPerPixel  { get; set; }
        public string OutputName    { get; set; }
    }
}