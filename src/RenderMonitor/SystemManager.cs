﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Timers;

namespace RenderMonitor {
    public class SystemManager : ISystemManager {
        private const double CpuReadInterval = 1000;
        private readonly PerformanceCounter _cpuCounter;
        private readonly PerformanceCounter _ramCounter;
        private readonly Timer _cpuReadTimer;

        private float _currentCpuValue;
        private bool _subscribedToTimer;

        public SystemManager() {
            _cpuReadTimer = new Timer() {
                Interval = CpuReadInterval,
                Enabled = false,
            };
            _subscribedToTimer = false;

            _cpuCounter = new PerformanceCounter();
            _cpuCounter.CategoryName = "Processor";
            _cpuCounter.CounterName = "% Processor Time";
            _cpuCounter.InstanceName = "_Total";
            _currentCpuValue = _cpuCounter.NextValue();

            _ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        }

        public string MachineName {
            get {
                return Environment.MachineName;
            }
        }

        public OperatingSystem MachineOS {
            get {
                return Environment.OSVersion;
            }
        }

        public double MachineCpuUsage {
            get {
                // WCF sometimes makes multiple client calls in one server method call.
                // Therefore, we need to prevent this counter from being measured
                // consecutively since this will give an incorrect CPU usage (always
                // 100%).
                //
                // To workaround this, we make sure that the performance counter is
                // read at most once per second.
                //
                if (_cpuReadTimer.Enabled) {
                    // The read interval has not elapsed so give the current value
                    return _currentCpuValue;
                }

                // Otherwise, it's been more than <interval> ms since we last read
                // CPU usage, read the new value and start the timer again.
                _currentCpuValue = _cpuCounter.NextValue();
                if (!_subscribedToTimer) {
                    _cpuReadTimer.Elapsed += (s, e) => {
                        _cpuReadTimer.Stop();
                        _cpuReadTimer.Enabled = false;
                    };
                }
                _cpuReadTimer.Enabled = true;
                _cpuReadTimer.Start();

                return _currentCpuValue;
            }
        }

        public double MachineAvailableRAM {
            get {
                return _ramCounter.NextValue();
            }
        }

        public string MachineIPv4Address {
            get {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                var ipv4 = host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                                           .FirstOrDefault();

                return (ipv4 != null) ? ipv4.ToString() : null;
            }
        }
    }
}