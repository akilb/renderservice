﻿using RenderMonitor.Def.Process;
using RenderService.Domain;

namespace RenderMonitor.Process {
    public class PbrtProcessFactory : IRenderProcessFactory {
        public IRenderProcess CreateProcess(RenderJob renderJob,
                                            int xRes,
                                            int yRes,
                                            int samplesPerPixel,
                                            string outFilePath) {
            return new PbrtProcess(renderJob, xRes, yRes, outFilePath);
        }
    }
}
