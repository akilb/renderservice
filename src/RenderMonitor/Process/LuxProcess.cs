﻿using System.Diagnostics.Contracts;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using RenderService.Domain;

namespace RenderMonitor.Process {
    public class LuxProcess : BaseRenderProcess {
        private const string LuxConsolePath = ".\\luxconsole.exe";

        private readonly string _arguments;

        public LuxProcess(RenderJob renderJob,
                          int xRes,
                          int yRes,
                          int haltSpp,
                          string outFilePath)
            : base(renderJob, xRes, yRes, haltSpp, outFilePath) {
            Contract.Requires(renderJob != null);

            // We want every LuxProcess for this job to use the same resume file.
            var resumeFileName = Path.GetFileNameWithoutExtension(renderJob.Scene) + ".flm";

            // For a luxconsole process we will always render the image at the
            // target resolution but we progressively increase the haltspp.
            //
            // So use the job resolution and then we convert the resolution of
            // the image output so that we're creating "progressively higher res"
            // images.
            _arguments = string.Format(
                            "--xresolution {0} --yresolution {1} --haltspp {2} --overrideresume {3} --output {4} {5}",
                            renderJob.XResolution,
                            renderJob.YResolution,
                            haltSpp,
                            Path.Combine(renderJob.JobFolder, resumeFileName),
                            Path.Combine(renderJob.JobFolder, outFilePath),
                            renderJob.Scene);
        }

        public override string ProcessPath {
            get {
                return LuxConsolePath;
            }
        }

        public override string ProcessArguments {
            get {
                return _arguments;
            }
        }

        protected override void ProcessStatusMessage(string message) {
            // NOOP for now.
        }

        protected override void OnAfterExecuteAsyncCompleted(RenderResultStatus status) {
            base.OnAfterExecuteAsyncCompleted(status);

            var outputFilePath = Path.Combine(_renderJob.JobFolder, _outFileName);

            if (_status != RenderResultStatus.Complete ||
                !File.Exists(outputFilePath)) {
                // process must have failed.
                _status = RenderResultStatus.Faulted;

                return;
            }

            // Now that we've achieved the samples per pixels for this render,
            // resize the image to the associated resolution for this render.
            using (var resizedImage = new Bitmap(_xResolution, _yResolution)) {
                using (var originalImage = Image.FromFile(outputFilePath))
                using (var graphics = Graphics.FromImage(resizedImage)) {
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    graphics.DrawImage(originalImage, 0, 0, _xResolution, _yResolution);
                }

                resizedImage.Save(outputFilePath, ImageFormat.Png);
            }
        }
    }
}
