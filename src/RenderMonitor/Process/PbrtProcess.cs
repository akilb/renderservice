﻿using RenderService.Domain;

namespace RenderMonitor.Process {
    public class PbrtProcess : BaseRenderProcess {
        private const string PbrtPath = ".\\pbrt.exe";

        private const string PbrtParsing = "Parsing";
        private const string PbrtRendering = "Rendering";
        private const string PbrtWritingImage = "WritingImage";

        private readonly string _processArguments;

        public PbrtProcess(RenderJob renderJob,
                           int xRes,
                           int yRes,
                           string outFileName)
            : base(renderJob, xRes, yRes, /*samplesPerPixel:*/-1, outFileName) {
            _processArguments
                = string.Format("--quiet --render_id {0} --outfile {1} --xres {2} --yres {3} {4}",
                                renderJob.ID,
                                outFileName,
                                xRes,
                                yRes,
                                renderJob.Scene);
        }

        public override string ProcessPath {
            get {
                return PbrtPath;
            }
        }

        public override string ProcessArguments {
            get {
                return _processArguments;
            }
        }

        protected override void ProcessStatusMessage(string statusMessage) {
            if (statusMessage == PbrtParsing) {
                _status = RenderResultStatus.Parsing;
            }
            else if (statusMessage == PbrtRendering) {
                _status = RenderResultStatus.Rendering;
            }
            else if (statusMessage == PbrtWritingImage) {
                _status = RenderResultStatus.WritingImage;
            }
        }
    }
}