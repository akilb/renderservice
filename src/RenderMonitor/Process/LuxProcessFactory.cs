﻿using RenderMonitor.Def.Process;
using RenderService.Domain;

namespace RenderMonitor.Process {
    public class LuxProcessFactory : IRenderProcessFactory {
        public IRenderProcess CreateProcess(RenderJob renderJob,
                                            int xRes,
                                            int yRes,
                                            int samplesPerPixel,
                                            string outFilePath) {
            return new LuxProcess(renderJob, xRes, yRes, samplesPerPixel, outFilePath);
        }
    }
}