﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Text;
using System.Threading.Tasks;
using RenderMonitor.Def.Process;
using RenderService.Domain;

namespace RenderMonitor.Process {
    public abstract class BaseRenderProcess : IRenderProcess {
        protected const string RenderServiceOutputHeader = "##RenderService##";

        protected readonly RenderJob _renderJob;
        protected readonly string _outFileName;
        protected readonly int _xResolution;
        protected readonly int _yResolution;
        protected readonly int _samplesPerPixel;

        private System.Diagnostics.Process _process;
        protected RenderResultStatus _status;

        private object _measurementLock;

        // Variables for memory measurements.
        private long _totalWorkingSet;
        private long _meanWorkingSet;
        private int _numberOfMeasurements;

        public BaseRenderProcess(RenderJob renderJob,
                                 int xRes,
                                 int yRes,
                                 int samplesPerPixel,
                                 string outFileName) {
            Contract.Requires(renderJob != null);

            _process = null;

            _renderJob = renderJob;
            _xResolution = xRes;
            _yResolution = yRes;
            _samplesPerPixel = samplesPerPixel;
            _outFileName = outFileName;
            _status = RenderResultStatus.Rendering;

            _totalWorkingSet = 0;
            _meanWorkingSet = -1;
            _numberOfMeasurements = 0;

            _measurementLock = new object();
        }

        public void ExecuteAsync() {
            if (_process != null) {
                throw new InvalidOperationException();
            }

            _process = new System.Diagnostics.Process {
                StartInfo = new ProcessStartInfo {
                    Arguments = ProcessArguments,
                    FileName = ProcessPath,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    StandardOutputEncoding = Encoding.UTF8,
                    StandardErrorEncoding = Encoding.UTF8,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false,
                    ErrorDialog = false,
                    WorkingDirectory = _renderJob.JobFolder
                }
            };
            _process.OutputDataReceived += ProcessOutputData;
            _process.ErrorDataReceived += ProcessError;

            var t = Task.Factory.StartNew(() => {
                _process.Start();
                _process.BeginOutputReadLine();
                _process.BeginErrorReadLine();

                MeasureMemoryUsage();

                _process.WaitForExit();

                _status = (_process.ExitCode == 0) ? RenderResultStatus.Complete : RenderResultStatus.Faulted;

                OnAfterExecuteAsyncCompleted(_status);

                FireOnExecuteAsyncCompleted();
            });
        }

        protected virtual void OnAfterExecuteAsyncCompleted(RenderResultStatus status) {
            if (!_process.HasExited ||
                (_status != RenderResultStatus.Complete && _status != RenderResultStatus.Faulted)) {
                throw new InvalidOperationException();
            }
        }

        public void Dispose() {
            if (_process != null) {
                _process.Dispose();
            }
        }

        public abstract string ProcessPath { get; }
        public abstract string ProcessArguments { get; }

        private void ProcessOutputData(object sender, DataReceivedEventArgs e) {
            if (string.IsNullOrEmpty(e.Data) ||
                !e.Data.Contains(RenderServiceOutputHeader)) {
                return;
            }

            // Report message is in this format:
            //      <output header>:<report id>:<status message>
            //
            var reportMessage = e.Data.Replace(RenderServiceOutputHeader, "").Trim();
            var tokens = reportMessage.Split(':');
            var statusMessage = tokens[2];

            ProcessStatusMessage(statusMessage);

            FireOnRenderResultChanged();
        }

        protected abstract void ProcessStatusMessage(string message);

        private void ProcessError(object sender, DataReceivedEventArgs e) {
            // NOOP for now.
        }

        private void MeasureMemoryUsage() {
            if (_process == null) {
                throw new InvalidOperationException();
            }

            if (_process.HasExited) {
                // Can only measure memory when the process is running.
                return;
            }

            var workingSet = _process.WorkingSet64;
            _totalWorkingSet += workingSet;
            _numberOfMeasurements++;
            _meanWorkingSet = _totalWorkingSet / _numberOfMeasurements;
        }

        private RenderResult GetCurrentRenderResult() {
            lock (_measurementLock) {
                if (_process == null) {
                    return new RenderResult {
                        RenderJob = _renderJob,
                        XResolution = _xResolution,
                        YResolution = _yResolution,
                        SamplesPerPixel = _samplesPerPixel,
                        ImageFile = _outFileName,
                        Status = _status
                    };
                }

                MeasureMemoryUsage();

                return new RenderResult {
                    RenderJob = _renderJob,
                    XResolution = _xResolution,
                    YResolution = _yResolution,
                    SamplesPerPixel = _samplesPerPixel,
                    ImageFile = _outFileName,
                    Status = _status,
                    CpuTimeMilliseconds = _process.TotalProcessorTime.TotalMilliseconds,
                    MeanWorkingSet = _meanWorkingSet
                };
            }
        }

        public event RenderResultChangedEventHandler OnRenderResultChanged;

        public event RenderResultChangedEventHandler OnExecuteAsyncCompleted;

        private void FireOnRenderResultChanged() {
            var temp = OnRenderResultChanged;
            if (temp != null) {
                temp(this, new RenderResultChangedEventArgs(GetCurrentRenderResult()));
            }
        }

        private void FireOnExecuteAsyncCompleted() {
            var temp = OnExecuteAsyncCompleted;
            if (temp != null) {
                temp(this, new RenderResultChangedEventArgs(GetCurrentRenderResult()));
            }
        }
    }
}
