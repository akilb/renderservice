﻿using System;

namespace RenderMonitor {
    public interface ISystemManager {
        double MachineAvailableRAM  { get; }
        double MachineCpuUsage      { get; }
        string MachineName          { get; }
        string MachineIPv4Address   { get; }
        OperatingSystem MachineOS   { get; }
    }
}
