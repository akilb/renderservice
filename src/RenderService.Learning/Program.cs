﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Encog.Engine.Network.Activation;
using Encog.ML.Data;
using Encog.Neural.Data.Basic;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation.Back;
using Encog.Neural.NeuralData;
using RenderService.Domain;
using Scene.Utils;

namespace RenderService.Learning {
    class Program {
        private const double MinError = 0.001d;
        private const int NTimes = 10;
        private const int NumberOfEpochs = 1000;

        static void Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine("Usage: RenderService.Learning.exe <data>.csv");
                return;
            }

            var jobs = ReadCsvFile(args[0]);

            var memDataSet = BuildDataSet(jobs, OutputSwitch.Memory);
            var cpuDataSet = BuildDataSet(jobs, OutputSwitch.CPU);

            using (var memWriter = new StreamWriter("memnetwork.csv"))
            using (var cpuWriter = new StreamWriter("cpunetwork.csv")) {

                for (int hl1 = 1; hl1 < 40; hl1++) {
                    for (int hl2 = 1; hl2 < 40; hl2++) {
                        for (double lr = 0.001d; lr < 0.01d; lr = lr + 0.005d) {
                            for (double mc = 0.001d; mc < 0.01d; mc = mc + 0.005d) {
                                var memNetwork = BuildNetwork(memDataSet, hl1, hl2);
                                var cpuNetwork = BuildNetwork(cpuDataSet, hl1, hl2);

                                var memTestError = CrossValidate(NTimes, memNetwork, memDataSet, NumberOfEpochs, lr, mc);
                                var cpuTestError = CrossValidate(NTimes, cpuNetwork, cpuDataSet, NumberOfEpochs, lr, mc);

                                WriteCsvLine(memWriter, hl1, hl2, lr, mc, memTestError);
                                WriteCsvLine(cpuWriter, hl1, hl2, lr, mc, cpuTestError);

                                Console.WriteLine("MemError:\t{0}\t\tCpuError:\t{1}", memTestError, cpuTestError);
                            }
                        }
                    }
                }
            }
        }

        private static void WriteCsvLine(StreamWriter writer,
                                         int hLayer1,
                                         int hLayer2,
                                         double lr,
                                         double mc,
                                         double error) {
            writer.Write(hLayer1);
            writer.Write(',');
            writer.Write(hLayer2);
            writer.Write(',');
            writer.Write(lr);
            writer.Write(',');
            writer.Write(mc);
            writer.Write(',');
            writer.WriteLine(error);
        }

        private static BasicNetwork BuildNetwork(BasicNeuralDataSet dataSet,
                                                 int hiddenLayerOneNeurons,
                                                 int hiddenLayerTwoNeurons) {
            var inputLayer = new BasicLayer(new ActivationSigmoid(), /*hasBias:*/true, dataSet.InputSize);
            var hiddenLayer1 = new BasicLayer(new ActivationSigmoid(), /*hasBias:*/true, hiddenLayerOneNeurons);
            var hiddenLayer2 = new BasicLayer(new ActivationSigmoid(), /*hasBias:*/true, hiddenLayerTwoNeurons);
            var outputLayer = new BasicLayer(new ActivationLinear(), /*hasBias:*/true, dataSet.IdealSize);

            var network = new BasicNetwork();
            network.AddLayer(inputLayer);
            network.AddLayer(hiddenLayer1);
            network.AddLayer(hiddenLayer2);
            network.AddLayer(outputLayer);
            network.Structure.FinalizeStructure();

            return network;
        }

        private static double CrossValidate(int nTimes,
                                            BasicNetwork network,
                                            BasicNeuralDataSet dataSet,
                                            int numberOfEpochs,
                                            double learningRate,
                                            double momentum) {
            int testSetSize = dataSet.Count() / nTimes;
            double totalTestError = 0d;
            for (int i = 0; i < nTimes; i++) {
                int firstTrainBatch = testSetSize * i;
                int testBatch = testSetSize * (i + 1);
                int secondTrainBatch = dataSet.Count() - firstTrainBatch - testBatch;
                var trainingPairs = new List<IMLDataPair>();
                trainingPairs.AddRange(dataSet.Take(firstTrainBatch));
                trainingPairs.AddRange(dataSet.Skip(testBatch).Take(secondTrainBatch));
                var testPairs = dataSet.Skip(firstTrainBatch)
                                       .Take(testBatch);
                var trainingSet = new BasicNeuralDataSet(trainingPairs.Select(p => p.InputArray).ToArray(),
                                                         trainingPairs.Select(p => p.IdealArray).ToArray());
                var testSet = new BasicNeuralDataSet(testPairs.Select(p => p.InputArray).ToArray(),
                                                     testPairs.Select(p => p.IdealArray).ToArray());
                network.Reset();

                var error =  RunTrainingAndGetTestError(trainingSet,
                                                        testSet,
                                                        network,
                                                        numberOfEpochs,
                                                        learningRate,
                                                        momentum);
                totalTestError += error;
            }

            return totalTestError / (double)nTimes;
        }

        private static double RunTrainingAndGetTestError(INeuralDataSet trainingSet,
                                                         INeuralDataSet testSet,
                                                         BasicNetwork network,
                                                         int numberOfEpochs,
                                                         double learningRate,
                                                         double momentum) {
            var train = new Backpropagation(network, trainingSet, learningRate, momentum);
            int epoch = 0;
            int consecutiveErrorIncrease = 0;
            do {
                var errBefore = train.Error;
                train.Iteration();

                if (train.Error > errBefore) {
                    consecutiveErrorIncrease++;
                }

                epoch++;
            } while ((epoch < numberOfEpochs) &&
                     (train.Error > MinError) &&
                     (consecutiveErrorIncrease < 20));

            return network.CalculateError(testSet);
        }

        private static string Print(double[] data) {
            var builder = new StringBuilder();
            builder.Append("[ ");
            foreach (var d in data) {
                builder.AppendFormat("{0} ", d);
            }
            builder.Append("]");

            return builder.ToString();
        }

        private enum OutputSwitch {
            Memory,
            CPU
        }

        private static BasicNeuralDataSet BuildDataSet(IEnumerable<RenderJob> jobs, OutputSwitch outputType) {
            var inputs = new double[jobs.Count()][];
            var outputs = new double[jobs.Count()][];
            for (int i = 0; i < jobs.Count(); i++) {
                var job = jobs.ElementAt(i);
                inputs[i] = new double[] {
                    job.XResolution,
                    job.YResolution,
                    job.SamplesPerPixel,
                    job.ProgressiveSteps,
                    (double)job.Description.Film,
                    (double)job.Description.Camera,
                    (double)job.Description.Sampler,
                    (double)job.Description.Renderer,
                    (double)job.Description.PixelFilter,
                    (double)job.Description.Accelerator,
                    (double)job.Description.VolumeIntegrator,
                    (double)job.Description.SurfaceIntegrator,
                    job.Description.SceneSize,
                    job.Description.DependenciesSize
                };

                double output;
                switch (outputType) {
                    case OutputSwitch.Memory:
                        // Get the mean of the MeanWorkingSets.
                        output = Math.Log((job.Results.Aggregate((long)0, (t, r) => t + r.MeanWorkingSet) / job.Results.Count));
                        break;
                    case OutputSwitch.CPU:
                        output = Math.Log((job.Results.Where(r => r.Status == RenderResultStatus.PartialResultComplete ||
                                                             r.Status == RenderResultStatus.Complete)
                                                      .Aggregate((double)0, (t, r) => t + r.CpuTimeMilliseconds)));
                        break;
                    default:
                        throw new InvalidOperationException();
                }
                outputs[i] = new double[] { output };
            }

            return new BasicNeuralDataSet(inputs, outputs);
        }

        private static IEnumerable<RenderJob> ReadCsvFile(string csvFile) {
            var jobs = new List<RenderJob>();
            using (var reader = new StreamReader(csvFile)) {
                while (!reader.EndOfStream) {
                    var jobText = reader.ReadLine();
                    if (string.IsNullOrWhiteSpace(jobText)) {
                        continue;
                    }

                    // Each job has the following format:
                    // xres, yres, spp, progressive_steps, film, camera, sampler,
                    // renderer, pixelfiler, accelerator, volumeintegrator,
                    // surfaceintegrator, scene_size, dependencies_size,
                    // mean_working_set, total_cpu_time
                    var fields = jobText.Split(',');
                    int xRes = int.Parse(fields[0]);
                    int yRes = int.Parse(fields[1]);
                    int spp = int.Parse(fields[2]);
                    int steps = int.Parse(fields[3]);
                    var film = (Film)int.Parse(fields[4]);
                    var camera = (Camera)int.Parse(fields[5]);
                    var sampler = (Sampler)int.Parse(fields[6]);
                    var renderer = (Renderer)int.Parse(fields[7]);
                    var pixelFilter = (PixelFilter)int.Parse(fields[8]);
                    var accel = (Accelerator)int.Parse(fields[9]);
                    var volInt = (VolumeIntegrator)int.Parse(fields[10]);
                    var surfInt = (SurfaceIntegrator)int.Parse(fields[11]);
                    var sceneSize = long.Parse(fields[12]);
                    var depSize = long.Parse(fields[13]);
                    var meanWorkingSet = long.Parse(fields[14]);
                    var cpuTime = double.Parse(fields[15]);

                    var job = new RenderJob {
                        JobFolder = "training data",
                        Scene = "training data",
                        Status = RenderJobStatus.Complete,
                        Owner = "training data",
                        CreatedDate = DateTime.Now,
                        XResolution = xRes,
                        YResolution = yRes,
                        SamplesPerPixel = spp,
                        ProgressiveSteps = steps,
                        Description = new RenderDescription {
                            Film = film,
                            Camera = camera,
                            Sampler = sampler,
                            Renderer = renderer,
                            PixelFilter = pixelFilter,
                            Accelerator = accel,
                            VolumeIntegrator = volInt,
                            SurfaceIntegrator = surfInt,
                            SceneSize = sceneSize,
                            DependenciesSize = depSize,
                            Path = "training data",
                            SamplesPerPixel = spp,
                            XResolution = xRes,
                            YResolution = yRes
                        },
                        Results = new List<RenderResult>()
                    };
                    job.Results.Add(new RenderResult {
                        ImageFile = "training data",
                        IsFinalResult = true,
                        RenderJob = job,
                        Status = RenderResultStatus.Complete,
                        CpuTimeMilliseconds = cpuTime,
                        MeanWorkingSet = meanWorkingSet,
                        SamplesPerPixel = spp,
                        XResolution = xRes,
                        YResolution = yRes
                    });

                    jobs.Add(job);
                }
            }

            return jobs;
        }
    }
}