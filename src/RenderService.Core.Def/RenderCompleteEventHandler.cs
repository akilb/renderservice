﻿using System;
using System.Diagnostics.Contracts;
using RenderService.Domain;

namespace RenderService.Core.Def {
    public delegate void RenderCompleteEventHandler(object sender, RenderCompleteEventArgs args);

    public class RenderCompleteEventArgs : EventArgs {
        private readonly RenderJob _job;

        public RenderCompleteEventArgs(RenderJob job) {
            Contract.Requires(job != null);

            _job = job;
        }

        public RenderJob RenderJob {
            get {
                return _job;
            }
        }
    }
}