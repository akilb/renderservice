﻿using System.ServiceModel;
using RenderService.Domain;

namespace RenderService.Core.Def {
    [ServiceContract(Namespace = "rf.services",
                     CallbackContract = typeof(IRenderMonitor),
                     SessionMode = SessionMode.Required)]
    public interface IRenderJobManager {
        [OperationContract(IsOneWay = true)]
        void RegisterMonitor();

        [OperationContract(IsOneWay = true)]
        void UnregisterMonitor();

        [OperationContract(IsOneWay = true)]
        void ReportRenderResult(RenderResult renderResult);
    }

    public interface IRenderMonitor {
        [OperationContract(IsOneWay = true)]
        void RenderAsync(RenderJob renderJob);

        [OperationContract(IsOneWay = false)]
        RenderMonitorInfo GetInformation();

        [OperationContract(IsOneWay = false)]
        void StopRender(RenderJob renderJob);
    }
}