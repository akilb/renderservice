﻿namespace RenderService.Core.Def {
    public static class Endpoints {
        public static string GetRenderJobManagerEndpoint(string address) {
            return string.Format("net.tcp://{0}:9080/RenderJobManager", address);
        }

        public static string GetRenderServiceEndpoint(string address) {
            return string.Format("net.tcp://{0}:9080/RenderService", address);
        }
    }
}
