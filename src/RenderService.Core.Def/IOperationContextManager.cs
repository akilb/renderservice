﻿namespace RenderService.Core.Def {
    public interface IOperationContextManager {
        TCallback GetCallbackChannel<TCallback>();
    }
}
