﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using RenderService.Domain;

namespace RenderService.Core.Def {
    [ServiceContract(Namespace = "rf.services",
                     SessionMode = SessionMode.Required)]
    public interface IRenderService {
        [OperationContract(IsOneWay = true)]
        void SubmitRender(RenderJob renderJob);
        [OperationContract(IsOneWay = true)]
        void StopRender(int jobId);

        [OperationContract(IsOneWay = false)]
        IEnumerable<RenderMonitorInfo> GetMonitorInformation();

        event RenderCompleteEventHandler OnRenderJobComplete;
        event EventHandler onNewMonitorRegistered;
    }
}