﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using RenderService.Core.Def;
using RenderService.Domain;

namespace RenderService.Core {
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
                     ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class RenderService : IRenderJobManager, IRenderService {
        private const int MonitorPollRateInMs = 120000;

        private readonly IRenderJobAssignmentMap _jobAssignmentMap;
        private readonly IOperationContextManager _opContextMgr;
        private readonly IRepository<RenderJob> _renderRepository;

        private readonly object _renderReportLock;
        private readonly Timer _monitorPollTimer;

        public RenderService(IOperationContextManager opContextMgr,
                             IRepository<RenderJob> renderRepository)
            : this(opContextMgr,
                   renderRepository,
                   new RenderJobAssignmentMap(),
                   disableRenderMonitorPolling: false) {
        }

        public RenderService(IOperationContextManager opContextMgr,
                             IRepository<RenderJob> renderRepository,
                             IRenderJobAssignmentMap assignmentMap,
                             bool disableRenderMonitorPolling) {
            Contract.Requires(opContextMgr != null);
            Contract.Requires(renderRepository != null);
            Contract.Requires(assignmentMap != null);

            _opContextMgr = opContextMgr;
            _renderRepository = renderRepository;
            _jobAssignmentMap = assignmentMap;

            _renderReportLock = new object();

            _monitorPollTimer = new Timer((state) => GetMonitorInformation());
            if (!disableRenderMonitorPolling) {
                _monitorPollTimer.Change(dueTime: 0,
                                         period: MonitorPollRateInMs);
            }
        }

        public void RegisterMonitor() {
            var monitor = _opContextMgr.GetCallbackChannel<IRenderMonitor>();

            _jobAssignmentMap.TryAddMonitor(monitor);

            FireOnNewMonitorRegistered();

            // We're sure that we have monitors now so dispatch any queued work.
            //
            using (var unitOfwork = new UnitOfWork()) {
                // NOTE: The ToList is essential here so that we pull all values from
                // the database. We save the db inside of the foreach so we need to be
                // sure that we aren't making db changes at the same time we are iterating
                // over a db query.
                var unassignedJobs = _renderRepository.FindAll(unitOfwork)
                                                      .Where(j => j.Status == RenderJobStatus.WaitingForDispatch)
                                                      .ToList();

                foreach (var job in unassignedJobs) {
                    TryDispatchRender(job);
                }
            }
        }

        public void UnregisterMonitor() {
            var monitor = _opContextMgr.GetCallbackChannel<IRenderMonitor>();

            RemoveMonitorAndReassignJobs(monitor);
        }

        public void StopRender(int jobId) {
            using (var unitOfWork = new UnitOfWork()) {
                var job = _renderRepository.Get(unitOfWork, jobId);
                if (job == null ||
                    job.Status == RenderJobStatus.WaitingForSubmission ||
                    job.Status == RenderJobStatus.Complete) {
                    // Job doesn't exist or is already stopped.
                    return;
                }

                if (job.Status == RenderJobStatus.InProgress) {
                    IRenderMonitor monitor = null;
                    if (!_jobAssignmentMap.TryGetAssignedMonitor(jobId, out monitor)) {
                        throw new InvalidOperationException("Why is the job InProgress if it isn't assigned?");
                    }

                    if (TryExecuteMonitorAction(monitor, m => m.StopRender(ShallowCopyRenderJob(job)))) {
                        _jobAssignmentMap.UnassignJobFromMonitor(monitor, jobId);
                    }
                }

                job.Status = RenderJobStatus.WaitingForSubmission;

                _renderRepository.Update(unitOfWork, job);
                _renderRepository.Save(unitOfWork);
            }
        }

        public void SubmitRender(RenderJob renderJob) {
            Contract.Requires(renderJob != null);
            Contract.Requires(renderJob.Status == RenderJobStatus.WaitingForSubmission);

            using (var unitOfWork = new UnitOfWork()) {
                var job = _renderRepository.Get(unitOfWork, renderJob.ID);
                bool jobExists = false;
                if (job == null) {
                    job = renderJob;
                }
                else {
                    jobExists = true;
                }

                job.Status = RenderJobStatus.WaitingForDispatch;

                if (jobExists) {
                    _renderRepository.Update(unitOfWork, job);
                }
                else {
                    _renderRepository.Add(unitOfWork, job);
                }

                _renderRepository.Save(unitOfWork);
            }

            TryDispatchRender(renderJob);
        }

        private bool TryDispatchRender(RenderJob renderJob) {
            IRenderMonitor monitor = null;
            if (!TryGetMonitorForDispatch(out monitor)) {
                // No registered monitors so the job will be processed
                // later.
                return false;
            }

            // REVIEW: could have some race conditions here if there are multiple requests...
            _jobAssignmentMap.AssignJobToMonitor(monitor, renderJob.ID);
            renderJob.Status = RenderJobStatus.InProgress;

            if (!TryExecuteMonitorAction(
                    monitor,
                    m => {
                        // IMPORTANT: We need to create a new RenderJob object here
                        // because WCF won't be able to serialize the entity that we
                        // retrieved from the database. Therefore, create a copy...
                        monitor.RenderAsync(ShallowCopyRenderJob(renderJob));
                    })
                ) {
                return false;
            }

            using (var unitOfWork = new UnitOfWork()) {
                var existingJob = _renderRepository.Get(unitOfWork, renderJob.ID);
                existingJob.Status = renderJob.Status;

                _renderRepository.Update(unitOfWork, existingJob);
                _renderRepository.Save(unitOfWork);
            }

            return true;
        }

        private bool TryExecuteMonitorAction(IRenderMonitor monitor,
                                             Action<IRenderMonitor> action) {
            try {
                action(monitor);

                return true;
            }
            catch {
                RemoveMonitorAndReassignJobs(monitor);

                return false;
            }
        }

        private void RemoveMonitorAndReassignJobs(IRenderMonitor monitor) {
            IEnumerable<int> jobsAssignedToMonitor = null;
            bool removed = _jobAssignmentMap.TryRemoveMonitor(monitor, out jobsAssignedToMonitor);

            Debug.Assert(removed);

            foreach (var jobId in jobsAssignedToMonitor) {
                RenderJob jobToReassign = null;
                using (var unitOfWork = new UnitOfWork()) {
                    jobToReassign = _renderRepository.Get(unitOfWork, jobId);
                    jobToReassign.Status = RenderJobStatus.WaitingForDispatch;
                    _renderRepository.Update(unitOfWork, jobToReassign);
                    _renderRepository.Save(unitOfWork);
                }

                TryDispatchRender(jobToReassign);
            }
        }

        public event RenderCompleteEventHandler OnRenderJobComplete;

        public event EventHandler onNewMonitorRegistered;

        public void ReportRenderResult(RenderResult result) {
            var reportingMonitor = _opContextMgr.GetCallbackChannel<IRenderMonitor>();
            if (!_jobAssignmentMap.ContainsMonitor(reportingMonitor)) {
                // This must be a faulted monitor that thinks it is still assigned some
                // job...
                // TODO: now that communication seems to be re-established..tell the monitor
                // to re-register.
                return;
            }

            // IMPORTANT: We need to synchronise when adding to the list of render
            // results so that we don't lose information.
            lock (_renderReportLock) {
                using (var unitOfWork = new UnitOfWork()) {
                    var existingJob = _renderRepository.Get(unitOfWork, result.RenderJob.ID);
                    var job = existingJob ?? result.RenderJob;

                    result.RenderJob = job;
                    job.Results.Add(result);

                    if (result.IsFinalResult) {
                        IRenderMonitor monitor = null;
                        _jobAssignmentMap.TryGetAssignedMonitor(job.ID, out monitor);
                        _jobAssignmentMap.UnassignJobFromMonitor(monitor, job.ID);
                        job.Status = RenderJobStatus.Complete;
                    }

                    if (existingJob == null) {
                        _renderRepository.Add(unitOfWork, job);
                    }
                    else {
                        _renderRepository.Update(unitOfWork, job);
                    }

                    _renderRepository.Save(unitOfWork);

                    if (result.IsFinalResult) {
                        FireOnRenderComplete(job);
                    }
                }
            }
        }

        public IEnumerable<RenderMonitorInfo> GetMonitorInformation() {
            var info = new List<RenderMonitorInfo>();
            foreach (var monitor in _jobAssignmentMap.Monitors) {
                TryExecuteMonitorAction(monitor,
                                        m => info.Add(m.GetInformation()));
            }

            return info;
        }

        private void FireOnRenderComplete(RenderJob job) {
            var temp = OnRenderJobComplete;
            if (temp != null) {
                temp(this, new RenderCompleteEventArgs(job));
            }
        }

        private void FireOnNewMonitorRegistered() {
            var temp = onNewMonitorRegistered;
            if (temp != null) {
                temp(this, EventArgs.Empty);
            }
        }

        private bool TryGetMonitorForDispatch(out IRenderMonitor monitor) {
            if (_jobAssignmentMap.Monitors.Count() == 1) {
                monitor = _jobAssignmentMap.Monitors.First();
            }
            else {
                // Get the monitor with the least jobs.
                // TODO: We could also take into account VM CPU usage and available
                // memory when we are making this decision.
                monitor = _jobAssignmentMap.Monitors
                                           .Select(m => {
                                               IEnumerable<int> assignedJobIds = null;
                                               _jobAssignmentMap.TryGetAssignedJobIds(m, out assignedJobIds);
                                               return new {
                                                   Monitor = m,
                                                   // NOTE: If we instead ask the monitor for information
                                                   // then we have to handle the case where the monitor
                                                   // call might fail.
                                                   JobIds = assignedJobIds
                                               };
                                           })
                                           .OrderBy(x => x.JobIds.Count())
                                           .Select(x => x.Monitor)
                                           .FirstOrDefault();
            }

            return (monitor != null);
        }

        private RenderJob ShallowCopyRenderJob(RenderJob job) {
            var copy = new RenderJob {
                CreatedDate = job.CreatedDate,
                ID = job.ID,
                JobFolder = job.JobFolder,
                Scene = job.Scene,
                Status = job.Status,
                XResolution = job.XResolution,
                YResolution = job.YResolution,
                SamplesPerPixel = job.SamplesPerPixel,
                ProgressiveSteps = job.ProgressiveSteps
            };

            if (job.Description != null) {
                copy.Description = new RenderDescription {
                    Accelerator = job.Description.Accelerator,
                    Camera = job.Description.Camera,
                    DependenciesSize = job.Description.DependenciesSize,
                    Film = job.Description.Film,
                    Path = job.Description.Path,
                    PixelFilter = job.Description.PixelFilter,
                    Renderer = job.Description.Renderer,
                    Sampler = job.Description.Sampler,
                    SceneSize = job.Description.SceneSize,
                    SurfaceIntegrator = job.Description.SurfaceIntegrator,
                    VolumeIntegrator = job.Description.VolumeIntegrator,
                    XResolution = job.Description.XResolution,
                    YResolution = job.Description.YResolution
                };
            }

            if (job.Results != null) {
                copy.Results = new List<RenderResult>();

                foreach (var r in job.Results) {
                    copy.Results.Add(new RenderResult {
                        XResolution = r.XResolution,
                        YResolution = r.YResolution,
                        SamplesPerPixel = r.SamplesPerPixel,
                        ID = r.ID,
                        ImageFile = r.ImageFile,
                        IsFinalResult = r.IsFinalResult,
                        MeanWorkingSet = r.MeanWorkingSet,
                        RenderJob = null,
                        Status = r.Status,
                        CpuTimeMilliseconds = r.CpuTimeMilliseconds
                    });
                }
            }

            return copy;
        }

        #region Static Members

        private static IRenderService _instance = null;

        public static IRenderService Instance {
            get {
                if (_instance == null) {
                    _instance = new RenderService(new OperationContextManager(),
                                                  new Repository<RenderJob>());
                }

                return _instance;
            }
        }

        #endregion

        private class OperationContextManager : IOperationContextManager {
            public TCallback GetCallbackChannel<TCallback>() {
                return OperationContext.Current.GetCallbackChannel<TCallback>();
            }
        }
    }
}
