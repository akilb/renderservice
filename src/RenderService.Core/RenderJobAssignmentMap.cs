﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using RenderService.Core.Def;
using System.Linq;

namespace RenderService.Core {
    public class RenderJobAssignmentMap : IRenderJobAssignmentMap {
        private readonly ConcurrentDictionary<IRenderMonitor, IList<int>> _map;

        public RenderJobAssignmentMap() {
            _map = new ConcurrentDictionary<IRenderMonitor, IList<int>>();
        }

        public IEnumerable<IRenderMonitor> Monitors {
            get {
                return _map.Keys;
            }
        }

        public bool TryAddMonitor(IRenderMonitor monitor) {
            return _map.TryAdd(monitor, new List<int>());
        }

        public bool TryRemoveMonitor(IRenderMonitor monitor,
                                     out IEnumerable<int> assignedJobIds) {
            IList<int> jobIds = null;
            var result = _map.TryRemove(monitor, out jobIds);

            assignedJobIds = jobIds;
            return result;
        }

        public bool ContainsMonitor(IRenderMonitor monitor) {
            return _map.ContainsKey(monitor);
        }

        public bool TryGetAssignedJobIds(IRenderMonitor monitor,
                                         out IEnumerable<int> assignedJobIds) {
            IList<int> jobIds = null;
            var result = _map.TryGetValue(monitor, out jobIds);

            assignedJobIds = jobIds;
            return result;
        }

        public bool TryGetAssignedMonitor(int jobId,
                                          out IRenderMonitor monitor) {
            monitor = _map.Where(pair => pair.Value.Contains(jobId))
                          .Select(pair => pair.Key)
                          .FirstOrDefault();

            return monitor != null;
        }

        public void AssignJobToMonitor(IRenderMonitor monitor, int jobId) {
            _map[monitor].Add(jobId);
        }

        public void UnassignJobFromMonitor(IRenderMonitor monitor, int jobId) {
            Contract.Requires(_map[monitor].Contains(jobId));

            _map[monitor].Remove(jobId);
        }
    }
}