﻿using System.Collections.Generic;
using RenderService.Core.Def;

namespace RenderService.Core {
    public interface IRenderJobAssignmentMap {
        bool ContainsMonitor(IRenderMonitor monitor);
        bool TryAddMonitor(IRenderMonitor monitor);
        bool TryRemoveMonitor(IRenderMonitor monitor,
                              out IEnumerable<int> assignedJobIds);
        bool TryGetAssignedJobIds(IRenderMonitor monitor,
                                  out IEnumerable<int> assignedJobIds);
        bool TryGetAssignedMonitor(int jobId, out IRenderMonitor monitor);
        void AssignJobToMonitor(IRenderMonitor monitor, int jobId);
        void UnassignJobFromMonitor(IRenderMonitor monitor, int jobId);

        IEnumerable<IRenderMonitor> Monitors { get; }
    }
}
