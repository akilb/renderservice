﻿using System.Data.Entity;

namespace RenderService.Domain {
    public class RenderServiceContext : DbContext {
        public DbSet<RenderJob> RenderJobs          { get; set; }
        public DbSet<RenderResult> RenderResults    { get; set; }
    }
}
