﻿using System;

namespace RenderService.Domain {
    public class RenderResult : RenderServiceEntity {
        public RenderResultStatus Status    { get; set; }
        public double CpuTimeMilliseconds   { get; set; }
        public long MeanWorkingSet          { get; set; }
        public int XResolution              { get; set; }
        public int YResolution              { get; set; }
        public int SamplesPerPixel          { get; set; }
        public string ImageFile             { get; set; }
        public bool IsFinalResult           { get; set; }
        
        public virtual RenderJob RenderJob  { get; set; }
    }
}