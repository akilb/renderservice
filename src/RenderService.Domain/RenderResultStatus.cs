﻿namespace RenderService.Domain {
    public enum RenderResultStatus {
        Parsing = 3,
        Rendering = 4,
        WritingImage = 5,
        PartialResultComplete = 6,
        PartialResultFaulted = 7,
        Complete = 8,
        Faulted = 9
    }
}