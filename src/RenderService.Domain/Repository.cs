﻿using System;
using System.Data;
using System.Diagnostics.Contracts;
using System.Linq;

namespace RenderService.Domain {
    public class Repository<TEntity> : IRepository<TEntity>
            where TEntity : RenderServiceEntity {
        public void Add(IUnitOfWork unitOfWork, TEntity entity) {
            Contract.Requires(unitOfWork != null);
            Contract.Requires(entity != null);

            unitOfWork.DataContext.Set<TEntity>().Add(entity);
        }

        public void Delete(IUnitOfWork unitOfWork, TEntity entity) {
            Contract.Requires(unitOfWork != null);
            Contract.Requires(entity != null);

            unitOfWork.DataContext.Set<TEntity>().Remove(entity);
        }

        public void Update(IUnitOfWork unitOfWork, TEntity entity) {
            Contract.Requires(unitOfWork != null);
            Contract.Requires(entity != null);

            unitOfWork.DataContext.Entry<TEntity>(entity).State = EntityState.Modified;
        }

        public void Save(IUnitOfWork unitOfWork) {
            Contract.Requires(unitOfWork != null);

            unitOfWork.DataContext.SaveChanges();
        }

        public TEntity Get(IUnitOfWork unitOfWork, long id) {
            Contract.Requires(unitOfWork != null);

            return unitOfWork.DataContext.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> Find(IUnitOfWork unitOfWork, Func<TEntity, bool> query) {
            Contract.Requires(unitOfWork != null);

            return unitOfWork.DataContext.Set<TEntity>().Where(query).AsQueryable();
        }

        public IQueryable<TEntity> FindAll(IUnitOfWork unitOfWork) {
            Contract.Requires(unitOfWork != null);

            return unitOfWork.DataContext.Set<TEntity>().AsQueryable();
        }
    }
}
