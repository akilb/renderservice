﻿namespace RenderService.Domain {
    public class UnitOfWork : IUnitOfWork {
        RenderServiceContext _dataContext = null;

        public RenderServiceContext DataContext {
            get {
                if (_dataContext == null) {
                    _dataContext = new RenderServiceContext();
                }

                return _dataContext;
            }
        }

        public void Dispose() {
            if (_dataContext != null) {
                _dataContext.Dispose();

                _dataContext = null;
            }
        }
    }
}
