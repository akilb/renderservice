﻿using Scene.Utils;

namespace RenderService.Domain {
    public class RenderDescription {
        public string Path                          { get; set; }
        public int XResolution                      { get; set; }
        public int YResolution                      { get; set; }
        public int SamplesPerPixel                  { get; set; }

        public Film Film                            { get; set; }
        public Camera Camera                        { get; set; }
        public Sampler Sampler                      { get; set; }
        public Renderer Renderer                    { get; set; }
        public PixelFilter PixelFilter              { get; set; }
        public Accelerator Accelerator              { get; set; }
        public VolumeIntegrator VolumeIntegrator    { get; set; }
        public SurfaceIntegrator SurfaceIntegrator  { get; set; }

        public long SceneSize                       { get; set; }
        public long DependenciesSize                { get; set; }
    }
}
