﻿using System;

namespace RenderService.Domain {
    public interface IUnitOfWork : IDisposable {
        RenderServiceContext DataContext { get; }
    }
}
