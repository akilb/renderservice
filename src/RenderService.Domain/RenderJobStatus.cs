﻿namespace RenderService.Domain {
    public enum RenderJobStatus {
        WaitingForSubmission,
        WaitingForDispatch,
        InProgress,
        Complete
    }
}