﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace RenderService.Domain {
    public class RenderJob : RenderServiceEntity {
        public RenderJob() {
            ProgressiveSteps = 1;
            Results = new List<RenderResult>();
        }

        [NotMapped]
        public string SceneFileName {
            get {
                return Path.GetFileName(Scene);
            }
        }

        public string Scene                     { get; set; }
        public string JobFolder                 { get; set; }
        public DateTime CreatedDate             { get; set; }
        public RenderDescription Description    { get; set; }
        public RenderJobStatus Status           { get; set; }

        [Required]
        public string Owner                     { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "X-Resolution")]
        public int XResolution { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Y-Resolution")]
        public int YResolution  { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Samples per Pixel")]
        public int SamplesPerPixel  { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Number of progressive rendering steps")]
        public int ProgressiveSteps { get; set; }

        public virtual ICollection<RenderResult> Results { get; set; }
    }
}