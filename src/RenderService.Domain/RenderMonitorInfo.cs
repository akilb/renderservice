﻿using System;
using System.Collections.Generic;

namespace RenderService.Domain {
    public class RenderMonitorInfo {
        public string MachineName                   { get; set; }
        public string MachineIPv4Addresss           { get; set; }
        public double MachineCpuUsage               { get; set; }
        public double MachineAvailableRAM           { get; set; }

        public IEnumerable<int> AssignedJobIds     { get; set; }
    }
}