﻿using System;
using System.Linq;

namespace RenderService.Domain {
    public interface IRepository<TEntity> where TEntity : RenderServiceEntity {
        void Add(IUnitOfWork unitOfWork, TEntity entity);
        void Delete(IUnitOfWork unitOfWork, TEntity entity);
        void Update(IUnitOfWork unitOfWork, TEntity entity);
        void Save(IUnitOfWork unitOfWork);

        TEntity Get(IUnitOfWork unitOfWork, long id);
        IQueryable<TEntity> Find(IUnitOfWork unitOfWork, Func<TEntity, bool> query);
        IQueryable<TEntity> FindAll(IUnitOfWork unitOfWork);
    }
}