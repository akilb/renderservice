﻿using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using RenderService.Domain;

namespace RenderService.Web.Models {
    public class RenderJobViewModel {
        public RenderJobViewModel() {
        }

        public RenderJob Job { get; set; }

        public string SceneName {
            get {
                return Path.GetFileName(Job.Scene);
            }
        }
    }
}