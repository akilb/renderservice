﻿using System.Diagnostics.Contracts;
using RenderService.Domain;

namespace RenderService.Web.Models {
    public class RenderMonitorInfoViewModel {
        private readonly RenderMonitorInfo _info;

        public RenderMonitorInfoViewModel(RenderMonitorInfo info) {
            Contract.Requires(info != null);

            _info = info;
        }

        public RenderMonitorInfo Info {
            get {
                return _info;
            }
        }
    }
}