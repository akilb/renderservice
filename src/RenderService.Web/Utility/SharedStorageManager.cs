﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Web;
using Scene.Utils.Packagers;

namespace RenderService.Web.Utility {
    public class SharedStorageManager : ISharedStorageManager {
        private readonly IScenePackager _packager;
        private readonly string _sharedStoragePath;

        public SharedStorageManager(string sharedStoragePath) {
            Contract.Requires(sharedStoragePath != null);

            _packager = new ScenePackager();
            _sharedStoragePath = sharedStoragePath;

            if (!Directory.Exists(_sharedStoragePath)) {
                Directory.CreateDirectory(_sharedStoragePath);
            }
        }

        public bool TryUnpackPackage(HttpPostedFileBase packageFile,
                                     out string scenePath,
                                     out string outputPath) {
            if (packageFile == null ||
                Path.GetExtension(packageFile.FileName) != ".lxspkg" ||
                packageFile.ContentLength <= 0) {
                // Invalid scene package.
                scenePath = null;
                outputPath = null;
                return false;
            }

            // Set up a folder in shared storage for this render:
            //  shared_storage_base/
            //      packageFolderPath/
            //          package.pbrtpkg
            //          scene.pbrt
            //          output/
            //
            var packageFolderPath
                    = Path.Combine(_sharedStoragePath,
                                   string.Format("{0}_{1}",
                                                 packageFile.FileName,
                                                 DateTime.UtcNow.ToString("U")
                                                    .Replace(' ', '_')
                                                    .Replace(':', '_')));
            var outputFolderPath = Path.Combine(packageFolderPath, "output");
            Directory.CreateDirectory(packageFolderPath);
            Directory.CreateDirectory(outputFolderPath);

            // Save the package in its storage folder.
            var fileName = Path.GetFileName(packageFile.FileName);
            var packagePath = Path.Combine(packageFolderPath, fileName);
            packageFile.SaveAs(packagePath);

            // Unzip it.
            _packager.Unpackage(packagePath, packageFolderPath);

            // and find the name of the scene contained in the package.
            var lxsFiles = Directory.EnumerateFiles(packageFolderPath,
                                                    "*.lxs",
                                                    SearchOption.TopDirectoryOnly)
                                    .Where(s => Path.GetExtension(s) == ".lxs");
            if (lxsFiles == null || lxsFiles.Count() != 1) {
                Debug.Fail("should only be one scene");
                scenePath = null;
                outputPath = null;
                return false;
            }

            scenePath = lxsFiles.First();
            outputPath = outputFolderPath;
            return true;
        }
    }
}