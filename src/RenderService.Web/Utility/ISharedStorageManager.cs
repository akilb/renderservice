﻿using System.Web;

namespace RenderService.Web.Utility {
    public interface ISharedStorageManager {
        bool TryUnpackPackage(HttpPostedFileBase packageFile,
                              out string scenePath,
                              out string outputPath);
    }
}