﻿using System.Diagnostics.Contracts;
using System.Linq;
using RenderService.Domain;
using Scene.Utils.Scanners;

namespace RenderService.Web.Utility {
    public class SceneAnalyzer : ISceneAnalyzer {
        private readonly ISceneScanner _scanner;
        private readonly IFileHelper _fileHelper;

        public SceneAnalyzer()
            : this(new SceneScanner(), new FileHelper()) {
        }

        public SceneAnalyzer(ISceneScanner scanner, IFileHelper fileHelper) {
            Contract.Requires(scanner != null);
            Contract.Requires(fileHelper != null);

            _scanner = scanner;
            _fileHelper = fileHelper;
        }

        public RenderDescription AnalyzeScene(string scenePath) {
            Contract.Requires(!string.IsNullOrEmpty(scenePath));

            var scene = _scanner.Scan(scenePath);
            long totalDependenciesSize = scene.Dependencies.Aggregate(
                                            seed: (long)0,
                                            func: (totalFileSize, dependency) =>
                                                        totalFileSize + _fileHelper.GetFileSize(dependency)
                                         );

            return new RenderDescription {
                Accelerator = scene.Accelerator,
                Camera = scene.Camera,
                DependenciesSize = totalDependenciesSize,
                Film = scene.Film,
                Path = scene.Path,
                PixelFilter = scene.PixelFilter,
                Renderer = scene.Renderer,
                Sampler = scene.Sampler,
                SceneSize = _fileHelper.GetFileSize(scene.Path),
                SurfaceIntegrator = scene.SurfaceIntegrator,
                VolumeIntegrator = scene.VolumeIntegrator,
                XResolution = scene.XResolution,
                YResolution = scene.YResolution,
                SamplesPerPixel = scene.HaltSamplesPerPixel
            };
        }
    }
}