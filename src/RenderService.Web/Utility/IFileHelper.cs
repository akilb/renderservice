﻿namespace RenderService.Web.Utility {
    public interface IFileHelper {
        long GetFileSize(string filePath);
    }
}