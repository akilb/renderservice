﻿using System.Diagnostics.Contracts;
using System.IO;

namespace RenderService.Web.Utility {
    public class FileHelper : IFileHelper {
        public long GetFileSize(string filePath) {
            Contract.Requires(File.Exists(filePath));

            return new FileInfo(filePath).Length;
        }
    }
}