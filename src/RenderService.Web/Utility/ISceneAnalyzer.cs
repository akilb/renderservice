﻿using RenderService.Domain;

namespace RenderService.Web.Utility {
    public interface ISceneAnalyzer {
        RenderDescription AnalyzeScene(string scenePath);
    }
}
