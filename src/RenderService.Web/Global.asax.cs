﻿using System;
using System.Data.Entity;
using System.ServiceModel;
using System.Web.Mvc;
using System.Web.Routing;
using RenderService.Core.Def;
using RenderService.Domain;

namespace RenderService.Web {
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication {
        private ServiceHost _duplex;

        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Dashboard",
                "Dashboard",
                new { controller = "Dashboard", action = "Dashboard" }
            );

            routes.MapRoute(
                "Renders",
                "Renders",
                new { controller = "Renders", action = "Index" }
            );

            routes.MapRoute(
                "Upload",
                "Renders/Upload",
                new { controller = "Renders", action = "Upload" }
            );

            routes.MapRoute(
                "DownloadResult",
                "Renders/{id}/DownloadResult/{resultId}",
                new {
                    controller = "Render",
                    action = "DownloadResult",
                    id = UrlParameter.Optional,
                    resultId = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                "Output",
                "Renders/Output/{id}",
                new { controller = "Renders", action = "Output", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "CpuGraph",
                "Renders/CpuGraphData/{id}",
                new { controller = "Renders", action = "CpuGraphData", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "MemoryGraph",
                "Renders/CpuGraphData/{id}",
                new { controller = "Renders", action = "MemoryGraphData", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Details",
                "Renders/{id}",
                new { controller = "Renders", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Monitors",
                "Admin/Monitors",
                new { controller = "Admin", action = "Monitors" }
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Renders", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RenderServiceContext>());

            _duplex = new ServiceHost(RenderService.Core.RenderService.Instance);
            _duplex.AddServiceEndpoint(typeof(IRenderJobManager),
                                       new NetTcpBinding() {
                                           SendTimeout = TimeSpan.MaxValue,
                                           ReceiveTimeout = TimeSpan.MaxValue
                                       },
                                       Endpoints.GetRenderJobManagerEndpoint("localhost"));
            _duplex.AddServiceEndpoint(typeof(IRenderJobManager),
                                       new NetTcpBinding() {
                                           SendTimeout = TimeSpan.MaxValue,
                                           ReceiveTimeout = TimeSpan.MaxValue
                                       },
                                       Endpoints.GetRenderServiceEndpoint("localhost"));
            _duplex.Open();
        }
    }
}