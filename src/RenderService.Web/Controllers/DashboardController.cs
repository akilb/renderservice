﻿using System.Web.Mvc;

namespace RenderService.Web.Controllers {
    public class DashboardController : Controller {
        //
        // GET: /Dashboard/
        [Authorize]
        public ActionResult Dashboard() {
            return View();
        }

        //
        // (Ajax)
        // GET: /Dashboard/GetTemplate
        [Authorize]
        public ActionResult GetTemplate() {
            return PartialView("_Template");
        }

        //
        // (Ajax)
        // GET: /Dashboard/ListWidgets/
        [Authorize]
        public JsonResult ListWidgets() {
            return Json(new {
                layout = "layout4",
                data = new object[] {
                    new {
                        title = "My Renders",
                        id = "rendersWidget",
                        column = "first",
                        editurl = "",
                        open = true,
                        url = "/Renders"
                    },
                    new {
                        title = "Details",
                        id = "detailsWidget",
                        column = "first",
                        editurl = "",
                        open = true,
                        url = "/Dashboard/SelectRender"
                    }
                }
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // (Ajax)
        // GET: /Dashboard/SelectRender/
        [Authorize]
        public PartialViewResult SelectRender() {
            return PartialView("_SelectRender");
        }
    }
}
