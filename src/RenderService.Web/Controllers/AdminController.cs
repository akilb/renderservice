﻿using System.Diagnostics.Contracts;
using System.Linq;
using System.Web.Mvc;
using RenderService.Core.Def;
using RenderService.Web.Models;
using System;

namespace RenderService.Web.Controllers
{
    public class AdminController : Controller
    {
        private IRenderService _renderService;

        public AdminController()
            : this(RenderService.Core.RenderService.Instance) {
        }

        public AdminController(IRenderService renderService) {
            Contract.Requires(renderService != null);

            _renderService = renderService;
        }

        //
        // GET: /Admin/Monitors
        public ActionResult Monitors()
        {
            var info = _renderService.GetMonitorInformation();
            return View(info.Select(i => new RenderMonitorInfoViewModel(i)));
        }

        //
        // POST: /Renders/
        [HttpPost]
        [Authorize]
        public JsonResult Monitors(string sidx, string sord, int page, int rows) {
            var info = _renderService.GetMonitorInformation();

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int totalRecords = info.Count();
            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            int id = 0;
            var jsonData = new {
                total = totalPages,
                page,
                records = totalRecords,
                rows = info.Select(m => new {
                    i = id++,
                    cell = new string[] {
                                m.MachineName,
                                m.MachineIPv4Addresss.ToString(),
                                m.MachineAvailableRAM.ToString(),
                                m.MachineCpuUsage.ToString(),
                                m.AssignedJobIds.Count().ToString(),
                            }
                }).ToArray()
            };

            return Json(jsonData);
        }
    }
}
