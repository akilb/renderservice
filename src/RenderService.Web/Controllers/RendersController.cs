﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RenderService.Core.Def;
using RenderService.Domain;
using RenderService.Web.Models;
using RenderService.Web.Utility;

namespace RenderService.Web.Controllers {
    public class RendersController : Controller {
        private readonly ISceneAnalyzer _analyzer;
        private readonly IRenderService _renderService;
        private readonly IRepository<RenderJob> _repository;

        private ISharedStorageManager _lazilyInitialisedStorageManager;

        public RendersController()
            : this(RenderService.Core.RenderService.Instance,
                   new Repository<RenderJob>(),
                   new SceneAnalyzer(),
                   storageManager: null) {
        }

        public RendersController(IRenderService renderService,
                                IRepository<RenderJob> repository,
                                ISceneAnalyzer analyzer,
                                ISharedStorageManager storageManager) {
            Contract.Requires(renderService != null);
            Contract.Requires(repository != null);
            Contract.Requires(analyzer != null);

            _renderService = renderService;
            _repository = repository;
            _analyzer = analyzer;
            _lazilyInitialisedStorageManager = storageManager;
        }

        private ISharedStorageManager StorageManager {
            get {
                if (_lazilyInitialisedStorageManager == null) {
                    _lazilyInitialisedStorageManager = new SharedStorageManager(Server.MapPath("~/App_Data/uploads"));
                }

                return _lazilyInitialisedStorageManager;
            }
        }

        //
        // GET: /Renders/
        //
        [Authorize]
        public ViewResultBase Index() {
            return Request.IsAjaxRequest() ? (ViewResultBase)PartialView("_RendersGrid") : View(); 
        }

        //
        // POST: /Renders/
        [HttpPost]
        [Authorize]
        public JsonResult Index(string sidx, string sord, int page, int rows) {
            using (var unitOfWork = new UnitOfWork()) {
                var userJobs = _repository.FindAll(unitOfWork)
                                          .Where(j => j.Owner == User.Identity.Name);

                // TODO: Can write an OrderBy extension method instead...
                bool asc = (sord == "asc");
                IEnumerable<RenderJob> orderedJobs = null;
                if (sidx == "SceneFileName") {
                    orderedJobs = asc ? userJobs.OrderBy(j => j.SceneFileName) :
                                        userJobs.OrderByDescending(j => j.SceneFileName);
                }
                else if (sidx == "Status") {
                    orderedJobs = asc ? userJobs.OrderBy(j => j.Status) :
                                        userJobs.OrderByDescending(j => j.Status);
                }
                else if (sidx == "XResolution") {
                    orderedJobs = asc ? userJobs.OrderBy(j => j.XResolution) :
                                        userJobs.OrderByDescending(j => j.XResolution);
                }
                else if (sidx == "YResolution") {
                    orderedJobs = asc ? userJobs.OrderBy(j => j.YResolution) :
                                        userJobs.OrderByDescending(j => j.YResolution);
                }
                else if (sidx == "SamplesPerPixel") {
                    orderedJobs = asc ? userJobs.OrderBy(j => j.SamplesPerPixel) :
                                        userJobs.OrderByDescending(j => j.SamplesPerPixel);
                }
                else {
                    orderedJobs = asc ? userJobs.OrderBy(j => j.ID) :
                                        userJobs.OrderByDescending(j => j.ID);
                }

                int pageIndex = Convert.ToInt32(page) - 1;
                int pageSize = rows;
                int totalRecords = userJobs.Count();
                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var pageOfJobss = orderedJobs.Skip(pageIndex * pageSize).Take(pageSize);

                var jsonData = new {
                    total = totalPages,
                    page,
                    records = totalRecords,
                    rows = pageOfJobss.Select(j => new {
                        i = j.ID,
                        cell = new string[] {
                                j.ID.ToString(),
                                j.SceneFileName,
                                j.Status.ToString(),
                                j.XResolution.ToString(),
                                j.YResolution.ToString(),
                                j.SamplesPerPixel.ToString()
                            }
                    }).ToArray()
                };

                return Json(jsonData);
            }
        }

        //
        // GET: /Render/Upload
        [Authorize]
        public ActionResult Upload() {
            return View(new RenderJobViewModel() { Job = new RenderJob() });
        }

        //
        // POST: /Render/Upload
        [HttpPost]
        [Authorize]
        public ActionResult Upload(HttpPostedFileBase packageFile) {
            string scenePath = null;
            string outputPath = null;
            if (!StorageManager.TryUnpackPackage(packageFile,
                                                 out scenePath,
                                                 out outputPath)) {
                return View("InvalidPackage");
            }

            var description = _analyzer.AnalyzeScene(scenePath);

            var renderJob = new RenderJob {
                Description = description,
                Scene = scenePath,
                JobFolder = outputPath,
                Status = RenderJobStatus.WaitingForSubmission,
                Owner = User.Identity.Name,
                CreatedDate = DateTime.Now,
                SamplesPerPixel = Math.Max(description.SamplesPerPixel, 1),
                XResolution = description.XResolution,
                YResolution = description.YResolution,
                ProgressiveSteps = 5
            };

            using (var unitOfWork = new UnitOfWork()) {
                _repository.Add(unitOfWork, renderJob);
                _repository.Save(unitOfWork);
            }

            return RedirectToAction("Details", new { id = renderJob.ID });
        }

        //
        // GET: /Renders/Output/id
        [Authorize]
        public ViewResultBase Output(int id) {
            var detailsView = Details(id);

            if (detailsView.ViewName != "_Details") {
                // Not found or invalid owner.
                return detailsView;
            }

            return PartialView("_Output");
        }

        //
        // GET: /Renders/id
        [Authorize]
        public ViewResultBase Details(int id) {
            RenderJob job = null;
            using (var unitOfwork = new UnitOfWork()) {
                job = _repository.Get(unitOfwork, id);

                if (job == null) {
                    return View("NotFound");
                }

                if (job.Owner != User.Identity.Name) {
                    return View("InvalidOwner");
                }

                // IMPORTANT: It's important that we initialize the results since
                // the db context will already be disposed by the time the view
                // starts rendering results...
                var initResults = job.Results.ToList();
                var vm = new RenderJobViewModel() { Job = job };

                return Request.IsAjaxRequest() ? (ViewResultBase)PartialView("_Details", vm) : View(vm);
            }
        }

        //
        // POST: /Renders/id
        [HttpPost]
        [Authorize]
        public ActionResult Details(int id, FormCollection form) {
            using (var unitOfWork = new UnitOfWork()) {
                var job = _repository.Get(unitOfWork, id);

                if (job.Owner != User.Identity.Name) {
                    return View("InvalidOwner");
                }

                var vm = new RenderJobViewModel() { Job = job };
                UpdateModel(vm);

                _repository.Update(unitOfWork, vm.Job);
                _repository.Save(unitOfWork);

                _renderService.SubmitRender(vm.Job);
            }

            return RedirectToAction("Index", "Renders");
        }

        //
        // (Ajax)
        // GET: /Renders/CpuGraphData/id
        [Authorize]
        public JsonResult CpuGraphData(int id) {
            var jsonData = GetGraphJsonData(
                            id,
                            results =>
                                results.Select(r => new string[] {
                                                        (results.IndexOf(r) + 1).ToString(),
                                                        r.CpuTimeMilliseconds.ToString()
                                                    }).ToArray());
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //
        // (Ajax)
        // GET: /Renders/MemoryGraphData/id
        [Authorize]
        public JsonResult MemoryGraphData(int id) {
            var jsonData = GetGraphJsonData(
                            id,
                            results =>
                                results.Select(r => new string[] {
                                                        (results.IndexOf(r) + 1).ToString(),
                                                        r.MeanWorkingSet.ToString()
                                                    }).ToArray());
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private object GetGraphJsonData(int id, Func<IList<RenderResult>, string[][]> dataCollector) {
            RenderJob job = null;
            using (var unitOfwork = new UnitOfWork()) {
                job = _repository.Get(unitOfwork, id);

                if (job == null ||
                    job.Owner != User.Identity.Name) {
                    return Json(new { });
                }

                var initResults = job.Results.ToList();
                var vm = new RenderJobViewModel() { Job = job };

                var jsonData = new {
                    data = dataCollector(initResults)
                };

                return jsonData;
            }
        }

        public ActionResult DownloadResult(int? id, int? resultId) {
            // TODO: Parameter error checking...

            using (var unitOfWork = new UnitOfWork()) {
                var job = _repository.Get(unitOfWork, id.Value);
                var result = job.Results.Where(r => r.ID == resultId).First();

                return File(Path.Combine(job.JobFolder, result.ImageFile), "image/png", result.ImageFile);
            }
        }

        public ActionResult StopRender(int id) {
            _renderService.StopRender(id);

            return RedirectToAction("Details", "Renders", new { id = id });
        }
    }
}