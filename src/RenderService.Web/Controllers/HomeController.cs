﻿using System.ServiceModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using RenderService.Core.Def;

namespace RenderService.Web.Controllers {
    public class HomeController : Controller {
        public ActionResult Index() {
            ViewBag.Message = "Render Service";

            return View();
        }

        public ActionResult About() {
            return View();
        }
    }
}
