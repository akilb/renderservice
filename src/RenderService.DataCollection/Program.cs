﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using RenderService.Core.Def;
using RenderService.Domain;
using Scene.Utils.Scanners;

namespace RenderService.DataCollection {
    class Program {
        static void Main(string[] args) {
            if (args.Length < 1) {
                Console.Write("usage: RenderService.DataCollection <scenes path> [-nocollect]");
                return;
            }

            if (!Directory.Exists(args[0])) {
                Console.WriteLine("directory does not exists: {0}", args[0]);
            }

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RenderServiceContext>());

            if (args.Length < 2 ||
                args[1] != "-nocollect") {
                var renderService = RenderService.Core.RenderService.Instance;
                var duplex = new ServiceHost(renderService);
                var endpoint = duplex.AddServiceEndpoint(
                                           typeof(IRenderJobManager),
                                           new NetTcpBinding() {
                                               SendTimeout = TimeSpan.MaxValue,
                                               ReceiveTimeout = TimeSpan.MaxValue
                                           },
                                           Endpoints.GetRenderJobManagerEndpoint("localhost"));
                duplex.Open();

                Console.WriteLine("##Running RenderService at: {0}", endpoint.Address.Uri.ToString());
                Console.WriteLine("Setup RenderMonitors and then press <ENTER> to begin collecting data.");
                Console.ReadLine();

                int noOfTimesToProcessScene = 1;
                if (args.Length == 3 &&
                    args[1] == "-eachscene") {
                    noOfTimesToProcessScene = int.Parse(args[2]);
                }

                // IMPORTANT: We're going through a lot of files so it's important to do one
                // at a time so that we don't run out of memory.
                var unduplicatedSceneFiles = Directory.EnumerateFiles(args[0],
                                                                      "*.lxs",
                                                                      SearchOption.AllDirectories);
                Console.WriteLine("Collecting data from {0} scenes (using each scene {1} times.)",
                                  unduplicatedSceneFiles.Count(),
                                  noOfTimesToProcessScene);

                var sceneFiles = new List<string>();
                for (int i = 0; i < noOfTimesToProcessScene; i++) {
                    sceneFiles.AddRange(unduplicatedSceneFiles);
                }
                var scenesLeft = sceneFiles.Count();
                var sceneFileEnumerator = sceneFiles.GetEnumerator();

                Console.WriteLine("## Rendering {0} scenes", sceneFiles.Count());

                bool finishedProcessing = false;
                renderService.OnRenderJobComplete += (s, e) => {
                    scenesLeft--;
                    Console.WriteLine("## Job {0} complete", e.RenderJob.ID);
                    Console.WriteLine("## {0} scenes left", scenesLeft);

                    if (scenesLeft == 0) {
                        // All renders have completed.
                        finishedProcessing = true;
                        return;
                    }

                    if (!sceneFileEnumerator.MoveNext()) {
                        // We've dispatched all of the scenes so just wait for
                        // all to finish.
                        return;
                    }

                    RenderScene(renderService, sceneFileEnumerator.Current);
                };
                renderService.onNewMonitorRegistered += (s, e) => {
                    // Dispatch a job if we have one so the new monitor gets
                    // some work for sure.
                    if (sceneFileEnumerator.MoveNext()) {
                        RenderScene(renderService, sceneFileEnumerator.Current);
                    }
                };

                // Our initial submit should be as many scenes as there are renderers
                // for.
                var monitorCount = renderService.GetMonitorInformation().Count();
                Console.WriteLine("##Detected {0} monitors", monitorCount);
                for (int i = 0; i < monitorCount; i++) {
                    if (!sceneFileEnumerator.MoveNext()) {
                        if (i == 0) {
                            Console.WriteLine("No scenes");
                            return;
                        }

                        break;
                    }
                    RenderScene(renderService, sceneFileEnumerator.Current);
                }

                while (!finishedProcessing) {
                    Thread.Sleep(TimeSpan.FromMinutes(3.0));
                }

                Console.WriteLine("## Finished Processing ##");
            }
            else {
                Console.WriteLine("## Skipped data collection.");
            }

            WriteCSVFile();
        }

        private static void RenderScene(IRenderService svc,
                                        string sceneFile) {
            var scanner = new SceneScanner();
            var rand = new Random();

            int xRes = rand.Next(20, 1920);
            int yRes = rand.Next(20, 1080);
            int haltspp = rand.Next(5, 50);
            int progressiveSteps = rand.Next(1, 20);

            var job = new RenderJob {
                Scene = sceneFile,
                Status = RenderJobStatus.WaitingForSubmission,
                Owner = "akilb",
                CreatedDate = DateTime.Now,
                Description = CreateSceneDescription(scanner.Scan(sceneFile)),
                JobFolder = Directory.GetCurrentDirectory(),
                XResolution = xRes,
                YResolution = yRes,
                SamplesPerPixel = haltspp,
                ProgressiveSteps = progressiveSteps
            };

            Console.WriteLine("## Rendering scene: {0}", job.Scene);
            Console.WriteLine("\tX-Resolution:\t{0}", job.XResolution);
            Console.WriteLine("\tY-Resolution:\t{0}", job.YResolution);
            Console.WriteLine("\tHaltSPP:\t\t{0}", job.SamplesPerPixel);
            Console.WriteLine("\tProgressive:\t{0}", job.ProgressiveSteps);

            svc.SubmitRender(job);
        }

        private static RenderDescription CreateSceneDescription(Scene.Utils.Scene scene) {
            long totalDependenciesSize = scene.Dependencies
                                              .Where(d => File.Exists(d))
                                              .Aggregate(
                                                    seed: (long)0,
                                                    func: (totalFileSize, dependency) =>
                                                                totalFileSize + new FileInfo(dependency).Length
                                              );

            return new RenderDescription {
                Accelerator = scene.Accelerator,
                Camera = scene.Camera,
                DependenciesSize = totalDependenciesSize,
                Film = scene.Film,
                Path = scene.Path,
                PixelFilter = scene.PixelFilter,
                Renderer = scene.Renderer,
                Sampler = scene.Sampler,
                SceneSize = new FileInfo(scene.Path).Length,
                SurfaceIntegrator = scene.SurfaceIntegrator,
                VolumeIntegrator = scene.VolumeIntegrator,
                XResolution = scene.XResolution,
                YResolution = scene.YResolution
            };
        }

        private static void WriteCSVFile() {
            Console.WriteLine("##Writing CSV file.");

            using (var unitOfWork = new UnitOfWork()) {
                var csvBuilder = new StringBuilder();
                var repo = new Repository<RenderJob>();
                foreach (var job in repo.FindAll(unitOfWork)) {
                    if (job.Status != RenderJobStatus.Complete) {
                        Console.WriteLine("Job ID {0} is not complete.", job.ID);
                        continue;
                    }

                    // Each job has the following format:
                    // xres, yres, spp, progressive_steps, film, camera, sampler,
                    // renderer, pixelfiler, accelerator, volumeintegrator,
                    // surfaceintegrator, scene_size, dependencies_size,
                    // mean_working_set, total_cpu_time
                    AppendCSVLine(
                        csvBuilder,
                        job.XResolution.ToString(),
                        job.YResolution.ToString(),
                        job.SamplesPerPixel.ToString(),
                        job.ProgressiveSteps.ToString(),
                        ((int)job.Description.Film).ToString(),
                        ((int)job.Description.Camera).ToString(),
                        ((int)job.Description.Sampler).ToString(),
                        ((int)job.Description.Renderer).ToString(),
                        ((int)job.Description.PixelFilter).ToString(),
                        ((int)job.Description.Accelerator).ToString(),
                        ((int)job.Description.VolumeIntegrator).ToString(),
                        ((int)job.Description.SurfaceIntegrator).ToString(),
                        job.Description.SceneSize.ToString(),
                        job.Description.DependenciesSize.ToString(),

                        // Get the mean of the MeanWorkingSets.
                        (job.Results.Aggregate((long)0, (t, r) => t + r.MeanWorkingSet) / job.Results.Count).ToString(),

                        // and the total cpu time (for the complete results
                        job.Results.Where(r => r.Status == RenderResultStatus.PartialResultComplete ||
                                               r.Status == RenderResultStatus.Complete)
                                   .Aggregate((double)0, (t, r) => t + r.CpuTimeMilliseconds).ToString()
                        );
                }

                File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), "data.csv"),
                                  csvBuilder.ToString());
            }
        }

        private static void AppendCSVLine(StringBuilder builder,
                                          params string[] fields) {
            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                builder.Append(field);

                if (i == fields.Length - 1) {
                    // end of the line...
                    builder.AppendLine();
                }
                else {
                    // more fields to come.
                    builder.Append(",");
                }
            }
        }
    }
}
