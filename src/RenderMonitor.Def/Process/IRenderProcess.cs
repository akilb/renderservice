﻿using System;

namespace RenderMonitor.Def.Process {
    public interface IRenderProcess : IDisposable {
        void ExecuteAsync();

        event RenderResultChangedEventHandler OnRenderResultChanged;
        event RenderResultChangedEventHandler OnExecuteAsyncCompleted;
    }
}
