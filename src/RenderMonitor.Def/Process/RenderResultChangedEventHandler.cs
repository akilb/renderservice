﻿using System;
using RenderService.Domain;

namespace RenderMonitor.Def.Process {
    public delegate void RenderResultChangedEventHandler(object sender, RenderResultChangedEventArgs result);

    public class RenderResultChangedEventArgs : EventArgs {
        private readonly RenderResult _result;

        public RenderResultChangedEventArgs(RenderResult result) {
            _result = result;
        }

        public RenderResult Result {
            get {
                return _result;
            }
        }
    }
}