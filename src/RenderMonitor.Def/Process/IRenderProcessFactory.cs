﻿using RenderService.Domain;

namespace RenderMonitor.Def.Process {
    public interface IRenderProcessFactory {
        IRenderProcess CreateProcess(RenderJob renderJob,
                                     int xRes,
                                     int yRes,
                                     int samplesPerPixel,
                                     string outFilePath);
    }
}
