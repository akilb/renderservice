﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RenderService.Core.Def;
using RenderService.Domain;
using RenderService.Web.Controllers;
using RenderService.Web.Models;
using RenderService.Web.Utility;

namespace RenderService.Web.Tests.Controllers {
    [TestClass]
    public class RenderControllerTest {
        private FormCollection CreateFakeRenderJobFormCollection() {
            var jobCollection = new FormCollection();
            jobCollection.Add("Job.XResolution", "100");
            jobCollection.Add("Job.YResolution", "100");
            jobCollection.Add("Job.SamplesPerPixel", "10");
            jobCollection.Add("Job.ProgressiveSteps", "1");
            jobCollection.Add("Job.Owner", "owner");

            return jobCollection;
        }

        private RendersController CreateController(IRenderService renderService = null,
                                                   IRepository<RenderJob> repo = null,
                                                   ISceneAnalyzer analyzer = null,
                                                   ISharedStorageManager storageManager = null,
                                                   string authenticatedUser = null,
                                                   bool isAjaxRequest = false) {
            var controller = new RendersController(
                renderService ?? new Mock<IRenderService>(MockBehavior.Strict).Object,
                repo ?? new Mock<IRepository<RenderJob>>(MockBehavior.Strict).Object,
                analyzer ?? new Mock<ISceneAnalyzer>(MockBehavior.Strict).Object,
                storageManager ?? new Mock<ISharedStorageManager>(MockBehavior.Strict).Object);

            var mockPrincipal = new Mock<IPrincipal>(MockBehavior.Strict);
            var mockIdentity = new Mock<IIdentity>(MockBehavior.Strict);
            var mockHttpContext = new Moq.Mock<HttpContextBase>();
            var mockHttpRequest = new Mock<HttpRequestBase>();
            mockHttpRequest.SetupGet(x => x.Headers)
                .Returns(isAjaxRequest ? new WebHeaderCollection() { {"X-Requested-With", "XMLHttpRequest"} } :
                                         new WebHeaderCollection()
            );
            mockHttpContext.SetupGet(x => x.Request).Returns(mockHttpRequest.Object);
            mockHttpContext.Setup(x => x.User).Returns(mockPrincipal.Object);
            mockPrincipal.Setup(u => u.Identity).Returns(mockIdentity.Object);
            mockIdentity.Setup(i => i.Name).Returns(authenticatedUser);

            controller.ControllerContext = new ControllerContext(mockHttpContext.Object,
                                                                 new RouteData(),
                                                                 controller);

            return controller;
        }

        [TestMethod]
        public void Index_Get_ShouldReturnView() {
            var controller = CreateController();

            var result = controller.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Index_Get_ShouldReturnPartialView_IfRequestIsAjaxRequest() {
            var controller = CreateController(isAjaxRequest: true);

            var result = (PartialViewResult)controller.Index();

            Assert.AreEqual("_RendersGrid", result.ViewName);
        }

        [TestMethod]
        public void Index_Post_ShouldReturnView_WithAllRenders_OwnedByAuthenticatedUser() {
            var authenticatedUser = "akilb";
            var jobOwnedByAuthenticatedUser = new RenderJob { ID = 1, Owner = authenticatedUser };
            var jobOwnedBySomeoneElse = new RenderJob { ID = 2, Owner = "someone else" };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob> { jobOwnedByAuthenticatedUser, jobOwnedBySomeoneElse }.AsQueryable());
            var controller = CreateController(repo: mockRepo.Object, authenticatedUser: authenticatedUser);

            var result = controller.Index("ID", "desc", 1, 10);
            var jsonResult = new JavaScriptSerializer().Serialize(result.Data);

            Assert.IsTrue(jsonResult.Contains("\"cell\":[\"1\""));
        }

        [TestMethod]
        public void Index_Post_ShouldReturnView_ThatExcludesRenders_OwnedByNonAuthenticatedUser() {
            var authenticatedUser = "akilb";
            var jobOwnedByAuthenticatedUser = new RenderJob { ID = 1, Owner = authenticatedUser };
            var jobOwnedBySomeoneElse = new RenderJob { ID = 2, Owner = "someone else" };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob> { jobOwnedByAuthenticatedUser, jobOwnedBySomeoneElse }.AsQueryable());
            var controller = CreateController(repo: mockRepo.Object, authenticatedUser: authenticatedUser);

            var result = controller.Index("ID", "desc", 1, 10);
            var jsonResult = new JavaScriptSerializer().Serialize(result.Data);

            Assert.IsFalse(jsonResult.Contains("\"cell\":[\"2\""));
        }

        [TestMethod]
        public void Upload_Get_ShouldReturnView() {
            var controller = CreateController();

            var result = (ViewResult)controller.Upload();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Upload_Post_ShouldReturnInvalidPackageView_WhenSharedStorageManagerFailsToUnpackPackage() {
            string outScene = null;
            string outOutput = null;
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out outScene,
                                                      out outOutput))
                       .Returns(false);
            var controller = CreateController(storageManager: mockStorage.Object);

            var result = controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object) as ViewResult;

            Assert.AreEqual("InvalidPackage", result.ViewName);
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_ToRepository() {
            string scene = "foo.lxs";
            string outputPath = "/foo/bar/";
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(new RenderDescription());
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>())).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldSaveJob_ToRepository() {
            string scene = "foo.lxs";
            string outputPath = "/foo/bar/";
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(new RenderDescription());
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>())).Verifiable();

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithExpectedDescription_ToRepository() {
            var scene = "foo.lxs";
            var outputPath = "/foo/bar/";
            var expectedDescription = new RenderDescription();
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(expectedDescription);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.Description == expectedDescription))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithExpectedScene_ToRepository() {
            var expectedScene = "foo.lxs";
            var outputPath = "/foo/bar/";
            var description = new RenderDescription();
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out expectedScene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(expectedScene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.Scene == expectedScene))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithExpectedOwner_ToRepository() {
            string expectedOwner = "akilb";
            var scene = "foo.lxs";
            var outputPath = "/foo/bar/";
            var description = new RenderDescription();
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.Owner == expectedOwner)))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: expectedOwner);

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithExpectedProgressiveSteps_ToRepository() {
            int expectedProgressiveSteps = 5;
            var scene = "foo.lxs";
            var outputPath = "/foo/bar/";
            var description = new RenderDescription();
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.ProgressiveSteps == expectedProgressiveSteps)))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithExpectedJobFolder_ToRepository() {
            var scene = "foo.lxs";
            var expectedOutputPath = "/foo/bar/";
            var description = new RenderDescription();
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out expectedOutputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.JobFolder == expectedOutputPath))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithWaitingForSubmissionRenderStatus_ToRepository() {
            var scene = "foo.lxs";
            var outputPath = "/foo/bar/";
            var description = new RenderDescription();
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForSubmission))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithDescriptionXResolution_ToRepository() {
            var scene = "foo.lxs";
            var outputPath = "/bar/";
            var expectedXRes = 10;
            var description = new RenderDescription { XResolution = expectedXRes };
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.XResolution == expectedXRes))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithDescriptionYResolution_ToRepository() {
            var scene = "foo.lxs";
            var outputPath = "/bar/";
            var expectedYRes = 12;
            var description = new RenderDescription { YResolution = expectedYRes };
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.YResolution == expectedYRes))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithDescriptionHaltSpp_ToRepository() {
            var scene = "foo.lxs";
            var outputPath = "/bar/";
            var expectedSamplesPerPixel = 13;
            var description = new RenderDescription { SamplesPerPixel = expectedSamplesPerPixel };
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.SamplesPerPixel == expectedSamplesPerPixel))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldAddJob_WithDefaultHaltSpp_IfDescriptionHaltSppIsInvalid() {
            var scene = "foo.lxs";
            var outputPath = "/bar/";
            var expectedSamplesPerPixel = 1;
            var description = new RenderDescription { SamplesPerPixel = -1 };
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(scene))
                        .Returns(description);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(),
                                      It.Is<RenderJob>(j => j.SamplesPerPixel == expectedSamplesPerPixel))).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Upload_Post_ShouldRedirectToSubmitView_WhenUploadSucceeds() {
            var scene = "foo.lxs";
            var outputPath = "/foo/bar/";
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(It.IsAny<string>()))
                        .Returns(new RenderDescription());
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            var result = (RedirectToRouteResult)controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            Assert.AreEqual("Details", result.RouteValues["action"]);
        }

        [TestMethod]
        public void Upload_Post_ShouldRedirectToSubmitView_WithIdOfNewJob() {
            var scene = "foo.lxs";
            var outputPath = "/foo/bar/";
            int expectedId = 55;
            var mockStorage = new Mock<ISharedStorageManager>(MockBehavior.Strict);
            var mockAnalyzer = new Mock<ISceneAnalyzer>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockStorage.Setup(s => s.TryUnpackPackage(It.IsAny<HttpPostedFileBase>(),
                                                      out scene,
                                                      out outputPath))
                       .Returns(true);
            mockAnalyzer.Setup(a => a.AnalyzeScene(It.IsAny<string>()))
                        .Returns(new RenderDescription());
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()))
                    .Callback<IUnitOfWork, RenderJob>((w, j) => j.ID = expectedId);
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var controller = CreateController(repo: mockRepo.Object,
                                              analyzer: mockAnalyzer.Object,
                                              storageManager: mockStorage.Object,
                                              authenticatedUser: "user");

            var result = (RedirectToRouteResult)controller.Upload(new Mock<HttpPostedFileBase>(MockBehavior.Strict).Object);

            Assert.AreEqual(expectedId, result.RouteValues["id"]);
        }

        [TestMethod]
        public void Details_Get_ShouldReturnNotFoundResult_IfIdDoesNotRelateToARenderJob() {
            int invalidId = 1;
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), invalidId))
                    .Returns<RenderJob>(null);
            var controller = CreateController(repo: mockRepo.Object);

            var result = (ViewResult)controller.Details(invalidId);

            Assert.AreEqual("NotFound", result.ViewName);
        }

        [TestMethod]
        public void Details_Get_ShouldReturnInvalidOwnerView_AuthenticatedUserDoesNotOwnThatJob() {
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(new RenderJob { Owner = "someone else" });
            var controller = CreateController(repo: mockRepo.Object,
                                              authenticatedUser: "someone");

            var result = (ViewResult)controller.Details(0);

            Assert.AreEqual("InvalidOwner", result.ViewName);
        }

        [TestMethod]
        public void Details_Get_ShouldReturnViewResult_WhenRequestIsNotAjaxRequest() {
            int id = 3;
            var job = new RenderJob { Owner = "user" };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            var controller = CreateController(repo: mockRepo.Object,
                                              authenticatedUser: job.Owner,
                                              isAjaxRequest: false);

            var result = controller.Details(id);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Details_Get_ShouldReturnPartialViewResult_WhenRequestIsAjaxRequest() {
            int id = 3;
            var job = new RenderJob { Owner = "user" };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            var controller = CreateController(repo: mockRepo.Object,
                                              authenticatedUser: job.Owner,
                                              isAjaxRequest: true);

            var result = controller.Details(id);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Details_Get_ShouldReturnPartialViewResult_WithDetailsFormName_WhenRequestIsAjaxRequest() {
            int id = 3;
            var job = new RenderJob { Owner = "user" };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            var controller = CreateController(repo: mockRepo.Object,
                                              authenticatedUser: job.Owner,
                                              isAjaxRequest: true);

            var result = (ViewResultBase)controller.Details(id);

            Assert.AreEqual("_Details", result.ViewName);
        }

        [TestMethod]
        public void Details_Get_ShouldReturnView_WithRenderJobViewModel_ContainingTheSpecifiedJob() {
            int id = 3;
            var expectedJob = new RenderJob { Owner = "user" };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(expectedJob);
            var controller = CreateController(repo: mockRepo.Object, authenticatedUser: expectedJob.Owner);

            var result = (ViewResult)controller.Details(id);

            Assert.AreEqual(expectedJob, ((RenderJobViewModel)result.Model).Job);
        }

        [TestMethod]
        public void Details_Post_ShouldReturnInvalidOwnerView_AuthenticatedUserDoesNotOwnThatJob() {
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(new RenderJob { Owner = "someone else" });
            var controller = CreateController(repo: mockRepo.Object,
                                              authenticatedUser: "someone");

            var result = (ViewResult)controller.Details(0, new FormCollection());

            Assert.AreEqual("InvalidOwner", result.ViewName);
        }

        [TestMethod]
        public void Details_Post_ShouldReturn_RedirectToRendersControllerIndexView() {
            int id = 4;
            var job = new RenderJob { Owner = "user" };
            var mockRenderSvc = new Mock<IRenderService>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRenderSvc.Setup(r => r.SubmitRender(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var controller = CreateController(mockRenderSvc.Object,
                                              mockRepo.Object,
                                              authenticatedUser: job.Owner);
            var form = CreateFakeRenderJobFormCollection();
            controller.ValueProvider = form.ToValueProvider();

            var result = (RedirectToRouteResult)controller.Details(id, form);

            Assert.AreEqual("Index", result.RouteValues["action"]);
        }

        [TestMethod]
        public void Details_Post_ShouldReturn_RedirectToRendersController() {
            int id = 4;
            var job = new RenderJob { Owner = "user" };
            var mockRenderSvc = new Mock<IRenderService>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRenderSvc.Setup(r => r.SubmitRender(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var controller = CreateController(mockRenderSvc.Object,
                                              mockRepo.Object,
                                              authenticatedUser: job.Owner);
            var form = CreateFakeRenderJobFormCollection();
            controller.ValueProvider = form.ToValueProvider();

            var result = (RedirectToRouteResult)controller.Details(id, form);

            Assert.AreEqual("Renders", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Details_Post_ShouldUpdateRenderJobInRepo() {
            int id = 4;
            var job = new RenderJob { Owner = "user" };
            var mockRenderSvc = new Mock<IRenderService>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRenderSvc.Setup(r => r.SubmitRender(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), job)).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var controller = CreateController(mockRenderSvc.Object,
                                              mockRepo.Object,
                                              authenticatedUser: job.Owner);
            var form = CreateFakeRenderJobFormCollection();
            controller.ValueProvider = form.ToValueProvider();

            var result = (RedirectToRouteResult)controller.Details(id, form);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Details_Post_ShouldSaveRenderJobInRepo() {
            int id = 4;
            var job = new RenderJob { Owner = "user" };
            var mockRenderSvc = new Mock<IRenderService>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRenderSvc.Setup(r => r.SubmitRender(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>())).Verifiable();
            var controller = CreateController(mockRenderSvc.Object,
                                              mockRepo.Object,
                                              authenticatedUser: job.Owner);
            var form = CreateFakeRenderJobFormCollection();
            controller.ValueProvider = form.ToValueProvider();

            var result = (RedirectToRouteResult)controller.Details(id, form);

            mockRepo.Verify();
        }

        [TestMethod]
        public void Details_Post_ShouldSubmitRenderJob() {
            int id = 4;
            var expectedJob = new RenderJob { Owner = "user" };
            var mockRenderSvc = new Mock<IRenderService>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRenderSvc.Setup(r => r.SubmitRender(expectedJob)).Verifiable();
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), id))
                    .Returns(expectedJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var controller = CreateController(mockRenderSvc.Object,
                                              mockRepo.Object,
                                              authenticatedUser: expectedJob.Owner);
            var form = CreateFakeRenderJobFormCollection();
            controller.ValueProvider = form.ToValueProvider();

            var result = (RedirectToRouteResult)controller.Details(id, form);

            mockRenderSvc.Verify();
        }

        [TestMethod]
        public void DownloadResult_Get_ShouldReturnTheResultsImageFile() {
            var jobId = 5;
            var renderId = 4;
            var expectedFilePath = "\\foo\\bar\\file.png";
            var job = new RenderJob {
                JobFolder = "\\foo\\bar",
                Results = new List<RenderResult> {
                    new RenderResult {
                        ID = renderId,
                        ImageFile = "file.png"
                    }
                }
            };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), jobId))
                    .Returns(job);
            var controller = CreateController(repo: mockRepo.Object);

            var result = (FilePathResult)controller.DownloadResult(jobId, renderId);

            Assert.AreEqual(expectedFilePath, result.FileName);
        }

        [TestMethod]
        public void DownloadResult_Get_ShouldUseTheImageFilenameasTheDownloadFileName() {
            var jobId = 5;
            var renderId = 4;
            var expectedDownloadFileName = "file.png";
            var job = new RenderJob {
                JobFolder = "\\foo\\bar",
                Results = new List<RenderResult> {
                    new RenderResult {
                        ID = renderId,
                        ImageFile = expectedDownloadFileName
                    }
                }
            };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), jobId))
                    .Returns(job);
            var controller = CreateController(repo: mockRepo.Object);

            var result = (FilePathResult)controller.DownloadResult(jobId, renderId);

            Assert.AreEqual(expectedDownloadFileName, result.FileDownloadName);
        }

        [TestMethod]
        public void StopRender_Get_ShouldTellTheRenderServiceToStopRender() {
            var expectedId = 20;
            var mockSvc = new Mock<IRenderService>(MockBehavior.Strict);
            mockSvc.Setup(rs => rs.StopRender(expectedId))
                   .Verifiable();
            var controller = CreateController(mockSvc.Object);

            var result = controller.StopRender(expectedId);

            mockSvc.Verify();
        }

        [TestMethod]
        public void StopRender_Get_ShouldRedirectTo_DetailsForThatAction() {
            var id = 20;
            var mockSvc = new Mock<IRenderService>(MockBehavior.Strict);
            mockSvc.Setup(rs => rs.StopRender(It.IsAny<int>()))
                   .Verifiable();
            var controller = CreateController(mockSvc.Object);

            var result = (RedirectToRouteResult)controller.StopRender(id);

            Assert.AreEqual("Details", result.RouteValues["action"]);
            Assert.AreEqual("Renders", result.RouteValues["controller"]);
            Assert.AreEqual(id, result.RouteValues["id"]);
        }
    }
}