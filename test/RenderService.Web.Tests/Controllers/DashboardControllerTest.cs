﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RenderService.Web.Controllers;

namespace RenderService.Web.Tests.Controllers {
    [TestClass]
    public class DashboardControllerTest {
        DashboardController CreateController() {
            return new DashboardController();
        }

        [TestMethod]
        public void Dashboard_Get_ShouldReturnAViewResult_ToDashboardView() {
            var controller = CreateController();

            var result = (ViewResult)controller.Dashboard();

            Assert.AreEqual(string.Empty, result.ViewName);
        }

        [TestMethod]
        public void SelectRender_Get_ShouldReturnSelectRenderPartialView() {
            var controller = CreateController();

            var result = (PartialViewResult)controller.SelectRender();

            Assert.AreEqual("_SelectRender", result.ViewName);
        }

        [TestMethod]
        public void SelectRender_Get_ShouldReturnTemplatePartialView() {
            var controller = CreateController();

            var result = (PartialViewResult)controller.GetTemplate();

            Assert.AreEqual("_Template", result.ViewName);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldReturnJsonResultThatAllowsGet() {
            var controller = CreateController();

            var result = controller.ListWidgets();

            Assert.AreEqual(JsonRequestBehavior.AllowGet, result.JsonRequestBehavior);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldUseExpectedLayout() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.AreEqual("layout4", data.layout);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldListDetailsWidget() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.AreEqual("detailsWidget", data.data[1].id);
            Assert.AreEqual("Details", data.data[1].title);
            Assert.AreEqual("first", data.data[1].column);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldListDetailsWidget_WithSelectRenderUrl() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.AreEqual("/Dashboard/SelectRender", data.data[1].url);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldListDetailsWidget_ToStartOpen() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.IsTrue(data.data[1].open);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldListRendersWidget() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.AreEqual("rendersWidget", data.data[0].id);
            Assert.AreEqual("My Renders", data.data[0].title);
            Assert.AreEqual("first", data.data[0].column);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldListRendersWidget_WithRendersUrl() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.AreEqual("/Renders", data.data[0].url);
        }

        [TestMethod]
        public void ListWidgets_Get_ShouldListRendersWidget_ToStartOpen() {
            var result = CreateController().ListWidgets();

            dynamic data = result.Data;

            Assert.IsTrue(data.data[0].open);
        }
    }
}
