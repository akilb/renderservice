﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RenderService.Core.Def;
using RenderService.Domain;
using RenderService.Web.Controllers;
using RenderService.Web.Utility;

namespace RenderService.Web.Tests.Controllers {
    [TestClass]
    public class RendersControllerTest {
        // TODO: Test the POST method.

        /*
        [TestMethod]
        public void Index_Post_ShouldReturnRenderJob_WithCorrectID() {
            int expectedID = 67;
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob> { new RenderJob { ID = expectedID } }.AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = controller.Index("ID", "asc", 0, 10) as JsonResult;

            dynamic data = result.Data;

            Assert.AreEqual(expectedID, data.rows[0].i);
        }

        [TestMethod]
        public void Index_Get_ShouldReturnView_WithAllRenders() {
            var expectedRenders = new List<RenderJobViewModel>() {
                new RenderJobViewModel(new RenderJob())
            };
            var mockRepo = new Mock<IRepository<RenderJob>>();
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(expectedRenders.Select(vm => vm.Job).AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = (ViewResult)controller.Index(page: null, pageSize: null);

            CollectionAssert.AreEquivalent(expectedRenders.Select(vm => vm.Job).ToList(),
                                          ((IEnumerable<RenderJobViewModel>)result.ViewData.Model).Select(vm => vm.Job).ToList());
        }

        [TestMethod]
        public void Index_Get_ShouldReturnView_WithRendersOrderedByID() {
            var expectedRenders = new List<RenderJobViewModel>() {
                new RenderJobViewModel(new RenderJob() { ID = 0 }),
                new RenderJobViewModel(new RenderJob() { ID = 1 }),
                new RenderJobViewModel(new RenderJob() { ID = 2 })
            };
            var mockRepo = new Mock<IRepository<RenderJob>>();
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(expectedRenders.Select(vm => vm.Job)
                                            .Reverse<RenderJob>()
                                            .AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = (ViewResult)controller.Index(page: null, pageSize: null);

            CollectionAssert.AreEqual(expectedRenders.Select(vm => vm.Job).ToList(),
                                      ((IEnumerable<RenderJobViewModel>)result.ViewData.Model).Select(vm => vm.Job).ToList());
        }

        [TestMethod]
        public void Index_Get_ShouldReturn_DefaultPageSizeOfRenderJobs_IfNoPageSizeIsGiven() {
            var renders = new List<RenderJobViewModel>();
            for (int i = 0; i < RendersController.DefaultPageSize * 3; i++) {
                renders.Add(new RenderJobViewModel());
            }
            var mockRepo = new Mock<IRepository<RenderJob>>();
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(renders.Select(vm => vm.Job).AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = (ViewResult)controller.Index(page: 1, pageSize: null);

            Assert.AreEqual(RendersController.DefaultPageSize,
                            ((IEnumerable<RenderJobViewModel>)result.ViewData.Model).Count());
        }

        [TestMethod]
        public void Index_Get_ShouldReturn_PageSizeItems_IfPageSizeIsSpecified() {
            var expectedNumberOfItems = 3;
            var renders = new List<RenderJobViewModel>();
            for (int i = 0; i < RendersController.DefaultPageSize * 3; i++) {
                renders.Add(new RenderJobViewModel());
            }
            var mockRepo = new Mock<IRepository<RenderJob>>();
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(renders.Select(vm => vm.Job).AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = (ViewResult)controller.Index(page: 1, pageSize: expectedNumberOfItems);

            Assert.AreEqual(expectedNumberOfItems,
                            ((IEnumerable<RenderJobViewModel>)result.ViewData.Model).Count());
        }

        [TestMethod]
        public void Index_Get_ShouldReturn_TheFirstPageOfItems_IfPageIsNotSpecified() {
            var firstPageJobs = new List<RenderJob>() {
                new RenderJob(),
                new RenderJob(),
                new RenderJob(),
            };
            var secondPageJobs = new List<RenderJob>() {
                new RenderJob(),
                new RenderJob(),
            };
            var expectedJobs = firstPageJobs;
            int pageSize = firstPageJobs.Count;
            var renders = new List<RenderJob>();
            renders.AddRange(firstPageJobs);
            renders.AddRange(secondPageJobs);
            var mockRepo = new Mock<IRepository<RenderJob>>();
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(renders.AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = (ViewResult)controller.Index(page: null, pageSize: pageSize);

            CollectionAssert.AreEqual(expectedJobs,
                                      ((IEnumerable<RenderJobViewModel>)result.ViewData.Model)
                                            .Select(vm => vm.Job)
                                            .ToList());
        }

        [TestMethod]
        public void Index_Get_ShouldReturn_TheSpecifiedPageOfItems_IfPageIsSpecified() {
            var firstPageJobs = new List<RenderJob>() {
                new RenderJob(),
                new RenderJob(),
                new RenderJob(),
            };
            var secondPageJobs = new List<RenderJob>() {
                new RenderJob(),
                new RenderJob(),
            };
            var expectedJobs = secondPageJobs;
            int pageSize = firstPageJobs.Count;
            var renders = new List<RenderJob>();
            renders.AddRange(firstPageJobs);
            renders.AddRange(secondPageJobs);
            var mockRepo = new Mock<IRepository<RenderJob>>();
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(renders.AsQueryable());
            var controller = CreateController(mockRepo.Object);

            var result = (ViewResult)controller.Index(page: 2, pageSize: pageSize);

            CollectionAssert.AreEqual(expectedJobs,
                                      ((IEnumerable<RenderJobViewModel>)result.ViewData.Model)
                                            .Select(vm => vm.Job)
                                            .ToList());
        }*/
    }
}
