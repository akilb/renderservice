﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RenderService.Core.Def;
using RenderService.Domain;
using RenderService.Web.Controllers;
using RenderService.Web.Models;

namespace RenderService.Web.Tests.Controllers {
    [TestClass]
    public class AdminControllerTest {
        private AdminController CreateController(IRenderService renderSvc = null) {
            return new AdminController(renderSvc ?? new Mock<IRenderService>(MockBehavior.Strict).Object);
        }

        [TestMethod]
        public void Monitors_Get_ShouldReturnRenderMonitorInfoVMs_ForEveryRenderMonitor() {
            var expectedMonitorInfo = new List<RenderMonitorInfo>() {
                new RenderMonitorInfo(),
                new RenderMonitorInfo()
            };
            var mockRenderSvc = new Mock<IRenderService>(MockBehavior.Strict);
            mockRenderSvc.Setup(r => r.GetMonitorInformation())
                         .Returns(expectedMonitorInfo);
            var controller = CreateController(mockRenderSvc.Object);

            var result = (ViewResult)controller.Monitors();

            CollectionAssert.AreEquivalent(expectedMonitorInfo,
                                           ((IEnumerable<RenderMonitorInfoViewModel>)(result.Model))
                                                .Select(vm => vm.Info)
                                                .ToList());
        }
    }
}
