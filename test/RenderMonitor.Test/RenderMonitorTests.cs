﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RenderMonitor.Def.Process;
using RenderService.Core.Def;
using RenderService.Domain;
using System;

namespace RenderMonitor.Test {
    [TestClass]
    public class RenderMonitorTests {
        private RenderMonitor CreateRenderMonitor(IRenderProcessFactory factory = null,
                                                  IRenderJobManager jobManager = null,
                                                  ISystemManager systemManager = null) {
            return new RenderMonitor(factory ?? new Mock<IRenderProcessFactory>(MockBehavior.Strict).Object,
                                     jobManager,
                                     systemManager ?? new Mock<ISystemManager>(MockBehavior.Strict).Object);
        }

        [TestMethod]
        public void Ctor_ShouldCreate_ARenderMonitor() {
            var monitor = new RenderMonitor();

            Assert.IsNotNull(monitor);
        }

        [TestMethod]
        public void RenderAsync_ShouldCreateNewRenderProcessWithTheCorrectArguments() {
            var expectedJob = new RenderJob { ID = 55, Scene = "foo.pbrt" };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(expectedJob,
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(expectedJob);

            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartNewRenderProcess() {
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt" };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockProcess.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithAppropriateResolution() {
            var expectedXRes = 500;
            var expectedYRes = 230;
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt", XResolution = expectedXRes, YResolution = expectedYRes };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   expectedXRes,
                                                   expectedYRes,
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockProcess.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithAppropriateSamplesPerPixel() {
            var expectedSpp = 500;
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 200,
                YResolution = 200,
                SamplesPerPixel = expectedSpp
            };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   expectedSpp,
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockProcess.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithAtLeastMinThresholdSamplesPerPixel() {
            var expectedSpp = 1;
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 200,
                YResolution = 200,
                SamplesPerPixel = 2,

                // Make sure that first progression divides to a value less than one
                ProgressiveSteps = 6
            };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   expectedSpp,
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithAtLeastMinThresholdXResolution() {
            var expectedXRes = 1;
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 2,
                YResolution = 2,
                SamplesPerPixel = 2,

                // Make sure that first progression divides to a value less than one
                ProgressiveSteps = 6
            };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   expectedXRes,
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithAtLeastMinThresholdYResolution() {
            var expectedYRes = 1;
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 2,
                YResolution = 2,
                SamplesPerPixel = 2,

                // Make sure that first progression divides to a value less than one
                ProgressiveSteps = 6
            };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   expectedYRes,
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithAppropriateOutputName() {
            var expectedOutputName = "foo_500x230.png";
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt", XResolution = 500, YResolution = 230 };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   expectedOutputName))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockProcess.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldStartRenderProcess_WithValuesBasedOnTheNumberOfProgressiveStepsInTheJob() {
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 500,
                YResolution = 250,
                SamplesPerPixel = 50,
                ProgressiveSteps = 5
            };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.Is<int>(i => i == (job.XResolution / job.ProgressiveSteps)),
                                                   It.Is<int>(i => i == (job.YResolution / job.ProgressiveSteps)),
                                                   It.Is<int>(i => i == (job.SamplesPerPixel / job.ProgressiveSteps)),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockProcess.VerifyAll();
        }

        [TestMethod]
        public void RenderAsync_ShouldResumeRenderProcess_WithProgressiveValuesAboveTheOnesThatHaveResults() {
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.lxs",
                XResolution = 10,
                YResolution = 10,
                SamplesPerPixel = 10,

                // Half of the progressive results are already computed
                Results = new List<RenderResult> {
                    new RenderResult {
                        XResolution = 5,
                        YResolution = 5,
                        SamplesPerPixel = 5,
                        ImageFile = "foo_5x5.lxs",
                        IsFinalResult = false,
                        Status = RenderResultStatus.PartialResultComplete
                    }
                }
            };

            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            // Although the progressive render should start at 5*5, do not compute
            // that result since it is already computed.
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(), 10, 10, 10, It.IsAny<string>()))
                       .Returns(mockProcess.Object)
                       .Verifiable();
            mockProcess.Setup(p => p.ExecuteAsync());
            var rm = CreateRenderMonitor(mockFactory.Object);

            rm.RenderAsync(job);

            mockFactory.Verify();
        }

        [TestMethod]
        public void RenderMonitor_ShouldReportRenderResult_WhenProcessExits() {
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt" };
            var expectedResult = new RenderResult { RenderJob = job };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockJobManager.Setup(m => m.ReportRenderResult(expectedResult));
            var rm = CreateRenderMonitor(mockFactory.Object, mockJobManager.Object);

            rm.RenderAsync(job);

            mockProcess.Raise(p => p.OnExecuteAsyncCompleted += null,
                              new RenderResultChangedEventArgs(expectedResult));

            mockJobManager.VerifyAll();
        }

        [TestMethod]
        public void RenderMonitor_ShouldReportRenderResult_WithCompleteStatus_WhenFinalProgressiveProcessExits() {
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt", XResolution = 50, YResolution = 50 };
            var result = new RenderResult { RenderJob = job, Status = RenderResultStatus.Complete };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockJobManager.Setup(m => m.ReportRenderResult(It.Is<RenderResult>(r => r.Status == RenderResultStatus.Complete)));
            var rm = CreateRenderMonitor(mockFactory.Object, mockJobManager.Object);

            rm.RenderAsync(job);

            mockProcess.Raise(p => p.OnExecuteAsyncCompleted += null,
                              new RenderResultChangedEventArgs(result));

            mockJobManager.VerifyAll();
        }

        [TestMethod]
        public void RenderMonitor_ShouldReportRenderResult_WithPartialResultFauled_WhenProgressiveProcessExitsWithError() {
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 50,
                YResolution = 50,
                ProgressiveSteps = 2
            };
            var result = new RenderResult { RenderJob = job, Status = RenderResultStatus.Faulted };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockJobManager.Setup(m => m.ReportRenderResult(It.Is<RenderResult>(r => r.Status == RenderResultStatus.PartialResultFaulted)));
            var rm = CreateRenderMonitor(mockFactory.Object, mockJobManager.Object);

            rm.RenderAsync(job);

            mockProcess.Raise(p => p.OnExecuteAsyncCompleted += null,
                              new RenderResultChangedEventArgs(result));

            mockJobManager.VerifyAll();
        }

        [TestMethod]
        public void RenderMonitor_ShouldReportRenderResult_WhenProcessSignalsResultIsUpdated() {
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt" };
            var expectedResult = new RenderResult { RenderJob = job };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockJobManager.Setup(m => m.ReportRenderResult(expectedResult));
            var rm = CreateRenderMonitor(mockFactory.Object, mockJobManager.Object);

            rm.RenderAsync(job);

            mockProcess.Raise(p => p.OnRenderResultChanged += null,
                              new RenderResultChangedEventArgs(expectedResult));

            mockJobManager.VerifyAll();
        }

        [TestMethod]
        public void Register_ShouldRegisterTheMonitorWithTheJobManager() {
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockJobManager.Setup(jm => jm.RegisterMonitor()).Verifiable();
            mockJobManager.Setup(jm => jm.UnregisterMonitor());
            using (var rm = new RenderMonitor(new Mock<IRenderProcessFactory>(MockBehavior.Strict).Object)) {
                rm.Register(mockJobManager.Object);

                mockJobManager.Verify();
            }
        }

        [TestMethod]
        public void Register_ShouldThrowInvalidOperationEx_IfAlreadyRegisteredToAJobManager() {
            var mockMgr = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockMgr.Setup(jm => jm.RegisterMonitor());
            var rm = CreateRenderMonitor();

            rm.Register(mockMgr.Object);

            bool exThrown = false;
            try {
                rm.Register(mockMgr.Object);
            }
            catch (InvalidOperationException e) {
                exThrown = true;
            }

            Assert.IsTrue(exThrown);
        }

        [TestMethod]
        public void Dispose_ShouldUnregisterMonitorFromTheSameJobManagerItRegisteredWith() {
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockJobManager.Setup(jm => jm.RegisterMonitor());
            mockJobManager.Setup(jm => jm.UnregisterMonitor()).Verifiable();
            using (var rm = CreateRenderMonitor(jobManager: null)) {
                rm.Register(mockJobManager.Object);
            }

            mockJobManager.Verify();
        }

        [TestMethod]
        public void Dispose_ShouldDisposeAllExistingProcesses() {
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt" };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);

            mockProcess.Setup(p => p.ExecuteAsync());
            mockProcess.Setup(p => p.Dispose());
            var rm = CreateRenderMonitor(mockFactory.Object, jobManager: null);
            rm.RenderAsync(job);

            rm.Dispose();

            mockProcess.VerifyAll();
        }

        [TestMethod]
        public void OnRenderProcessComplete_ShouldContinueProgressivelyRendering_AtNextResolution() {
            var job = new RenderJob {
                ID = 55,
                Scene = "foo.pbrt",
                XResolution = 400,
                YResolution = 400,
                ProgressiveSteps = 2
            };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockJobManager.Setup(m => m.ReportRenderResult(It.IsAny<RenderResult>()));
            var rm = CreateRenderMonitor(mockFactory.Object, mockJobManager.Object);

            rm.RenderAsync(job);

            // Set up for the re-render.
            mockFactory.Setup(f => f.CreateProcess(job,
                                                   400,
                                                   400,
                                                   It.IsAny<int>(),
                                                   "foo_400x400.png"))
                       .Returns(mockProcess.Object)
                       .Verifiable();
            mockProcess.Setup(p => p.ExecuteAsync());

            mockProcess.Raise(p => p.OnExecuteAsyncCompleted += null,
                              new RenderResultChangedEventArgs(new RenderResult()));

            mockFactory.Verify();
        }

        [TestMethod]
        public void OnRenderProcessComplete_ShouldSetIsFinalResult_InRenderResult_WhenProgressiveRenderCompletes() {
            var job = new RenderJob { ID = 55, Scene = "foo.pbrt" };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockJobManager = new Mock<IRenderJobManager>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockJobManager.Setup(m => m.ReportRenderResult(It.Is<RenderResult>(r => r.IsFinalResult == true)))
                          .Verifiable();
            var rm = CreateRenderMonitor(mockFactory.Object, mockJobManager.Object);

            rm.RenderAsync(job);

            mockProcess.Raise(p => p.OnExecuteAsyncCompleted += null,
                              new RenderResultChangedEventArgs(new RenderResult() { RenderJob = job }));

            mockJobManager.Verify();
        }

        [TestMethod]
        public void GetInformation_ShouldReturnRenderMonInfoObject_WithAppropriateMachineName() {
            var expectedMachineName = "foo";
            var mockSysMgr = new Mock<ISystemManager>(MockBehavior.Strict);
            mockSysMgr.Setup(s => s.MachineName).Returns(expectedMachineName);
            mockSysMgr.Setup(s => s.MachineCpuUsage).Returns(1.0);
            mockSysMgr.Setup(s => s.MachineAvailableRAM).Returns(1000);
            mockSysMgr.Setup(s => s.MachineIPv4Address).Returns("ip");
            var rm = CreateRenderMonitor(systemManager: mockSysMgr.Object);

            var actualMachineName = rm.GetInformation().MachineName;

            Assert.AreEqual(expectedMachineName, actualMachineName);
        }

        [TestMethod]
        public void GetInformation_ShouldReturnRenderMonInfoObject_WithAppropriateCpuUsage() {
            var expectedCpuUsage = 4.0;
            var mockSysMgr = new Mock<ISystemManager>(MockBehavior.Strict);
            mockSysMgr.Setup(s => s.MachineName).Returns("foo");
            mockSysMgr.Setup(s => s.MachineCpuUsage).Returns(expectedCpuUsage);
            mockSysMgr.Setup(s => s.MachineAvailableRAM).Returns(1000);
            mockSysMgr.Setup(s => s.MachineIPv4Address).Returns("ip");
            var rm = CreateRenderMonitor(systemManager: mockSysMgr.Object);

            var actualCpuUsage = rm.GetInformation().MachineCpuUsage;

            Assert.AreEqual(expectedCpuUsage, actualCpuUsage);
        }

        [TestMethod]
        public void GetInformation_ShouldReturnRenderMonInfoObject_WithAppropriateAvailableRAM() {
            var expectedRAM = 400.0;
            var mockSysMgr = new Mock<ISystemManager>(MockBehavior.Strict);
            mockSysMgr.Setup(s => s.MachineName).Returns("foo");
            mockSysMgr.Setup(s => s.MachineCpuUsage).Returns(1.0);
            mockSysMgr.Setup(s => s.MachineAvailableRAM).Returns(expectedRAM);
            mockSysMgr.Setup(s => s.MachineIPv4Address).Returns("ip");
            var rm = CreateRenderMonitor(systemManager: mockSysMgr.Object);

            var actualRAM = rm.GetInformation().MachineAvailableRAM;

            Assert.AreEqual(expectedRAM, actualRAM);
        }

        [TestMethod]
        public void GetInformation_ShouldReturnRenderMonInfoObject_WithAppropriateAvailableIP() {
            var expectedIP = "ipaddress";
            var mockSysMgr = new Mock<ISystemManager>(MockBehavior.Strict);
            mockSysMgr.Setup(s => s.MachineName).Returns("foo");
            mockSysMgr.Setup(s => s.MachineCpuUsage).Returns(1.0);
            mockSysMgr.Setup(s => s.MachineAvailableRAM).Returns(400);
            mockSysMgr.Setup(s => s.MachineIPv4Address).Returns(expectedIP);
            var rm = CreateRenderMonitor(systemManager: mockSysMgr.Object);

            var actualIP = rm.GetInformation().MachineIPv4Addresss;

            Assert.AreEqual(expectedIP, actualIP);
        }

        [TestMethod]
        public void GetInformation_ShouldReturnRenderMonInfoObject_WithAppropriateJobIds() {
            int expectedId = 4;
            var job = new RenderJob { ID = expectedId, Scene = "foo.pbrt" };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            var mockSysMgr = new Mock<ISystemManager>(MockBehavior.Strict);
            mockSysMgr.Setup(s => s.MachineName).Returns("foo");
            mockSysMgr.Setup(s => s.MachineCpuUsage).Returns(1.0);
            mockSysMgr.Setup(s => s.MachineAvailableRAM).Returns(400);
            mockSysMgr.Setup(s => s.MachineIPv4Address).Returns("ip");
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);

            mockProcess.Setup(p => p.ExecuteAsync());
            mockProcess.Setup(p => p.Dispose());
            var rm = CreateRenderMonitor(mockFactory.Object, systemManager: mockSysMgr.Object);
            rm.RenderAsync(job);

            var actualJobIds = rm.GetInformation().AssignedJobIds;

            Assert.IsTrue(actualJobIds.Count() == 1);
            Assert.AreEqual(expectedId, actualJobIds.First());
        }

        [TestMethod]
        public void StopRender_ShouldStopRenderProcessForTheJob() {
            var job = new RenderJob { ID = 5, Scene = "foo.pbrt" };
            var mockProcess = new Mock<IRenderProcess>(MockBehavior.Strict);
            var mockFactory = new Mock<IRenderProcessFactory>(MockBehavior.Strict);
            mockFactory.Setup(f => f.CreateProcess(It.IsAny<RenderJob>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<int>(),
                                                   It.IsAny<string>()))
                       .Returns(mockProcess.Object);
            mockProcess.Setup(p => p.ExecuteAsync());
            mockProcess.Setup(p => p.Dispose())
                       .Verifiable();
            var rm = CreateRenderMonitor(mockFactory.Object);
            rm.RenderAsync(job);

            rm.StopRender(job);

            mockProcess.Verify();
        }
    }
}