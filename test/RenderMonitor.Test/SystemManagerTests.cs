﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RenderMonitor.Test {
    [TestClass]
    public class SystemManagerTests {
        [TestMethod]
        public void MachineName_Test() {
            var mgr = new SystemManager();

            Assert.IsFalse(string.IsNullOrEmpty(mgr.MachineName));
        }

        [TestMethod]
        public void MachineOS_Test() {
            var mgr = new SystemManager();

            Assert.IsNotNull(mgr.MachineOS);
        }

        [TestMethod]
        public void MachineAvailableRAM_Test() {
            var mgr = new SystemManager();

            Assert.IsInstanceOfType(mgr.MachineAvailableRAM, typeof(double));
        }

        [TestMethod]
        public void MachineCPUUsage_Test() {
            var mgr = new SystemManager();

            Assert.IsInstanceOfType(mgr.MachineCpuUsage, typeof(double));
        }

        [TestMethod]
        public void MachineIPAddress_Test() {
            var mgr = new SystemManager();

            Assert.IsFalse(string.IsNullOrEmpty(mgr.MachineIPv4Address));
        }
    }
}
