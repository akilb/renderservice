﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RenderMonitor.Process;
using RenderService.Domain;

namespace RenderMonitor.Test.Process
{
    [TestClass()]
    public class LuxProcessFactoryTest {
        [TestMethod]
        public void Ctor_Test() {
            var factory = new LuxProcessFactory();

            Assert.IsNotNull(factory);
        }

        [TestMethod]
        public void CreateRenderProcess_ShouldReturn_ALuxProcess() {
            var fac = new LuxProcessFactory();

            var proc = fac.CreateProcess(new RenderJob { Scene = "foo.lxs", JobFolder = "/foo/bar/" },
                                         1,
                                         1,
                                         1,
                                         "foo.png");

            Assert.IsInstanceOfType(proc, typeof(LuxProcess));
        }
    }
}
