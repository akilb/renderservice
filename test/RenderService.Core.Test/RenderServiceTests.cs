﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RenderService.Core.Def;
using RenderService.Domain;

namespace RenderService.Core.Test {
    [TestClass]
    public class RenderServiceTests {
        private IList<RenderJob> CreateRenderInfo(IEnumerable<string> scenes) {
            int id = 50;
            return scenes.Select(s => CreateRenderInfo(s, id++)).ToList();
        }

        private RenderJob CreateRenderInfo(string scene, int id = 40) {
            return new RenderJob { ID = id, Scene = scene };
        }

        private RenderService CreateRenderService(IOperationContextManager opMgr = null,
                                                  IRepository<RenderJob> repo = null,
                                                  IRenderJobAssignmentMap assignmentMap = null) {
            return new RenderService(opMgr ?? new Mock<IOperationContextManager>(MockBehavior.Strict).Object,
                                     repo ?? new Mock<IRepository<RenderJob>>(MockBehavior.Strict).Object,
                                     assignmentMap ?? new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict).Object,
                                     disableRenderMonitorPolling: true);
        }

        [TestMethod]
        public void RegisterMonitor_ShouldAddMonitorToAssignmentMap() {
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockAssignmentMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>().AsQueryable());
            mockAssignmentMap.Setup(m => m.TryAddMonitor(mockMonitor.Object))
                             .Returns(true)
                             .Verifiable();
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockAssignmentMap.Object);

            svc.RegisterMonitor();

            mockAssignmentMap.Verify();
        }

        [TestMethod]
        public void RegisterMonitor_ShouldFireMonitorRegisteredEvent() {
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockAssignmentMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>().AsQueryable());
            mockAssignmentMap.Setup(m => m.TryAddMonitor(mockMonitor.Object))
                             .Returns(true);
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockAssignmentMap.Object);
            var notificationSent = false;
            svc.onNewMonitorRegistered += (s, e) => notificationSent = true;

            svc.RegisterMonitor();

            Assert.IsTrue(notificationSent);
        }

        [TestMethod]
        public void RegisterMonitor_ShouldDispatchJobsWaitingForDispatch_WhenAMonitorRegisters() {
            var queuedJob = new RenderJob { Status = RenderJobStatus.WaitingForDispatch };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { queuedJob }.AsQueryable());
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(queuedJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMonitor.Setup(c => c.RenderAsync(It.Is<RenderJob>(j => j.ID == queuedJob.ID)));
            mockMap.Setup(m => m.TryAddMonitor(mockMonitor.Object)).Returns(true);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));

            var svc = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            svc.RegisterMonitor();

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void RegisterMonitor_ShouldNotDispatchWaitingForSubmissionJobs_WhenAMonitorRegisters() {
            var completeJob = new RenderJob { Status = RenderJobStatus.WaitingForSubmission };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { completeJob }.AsQueryable());
            mockMap.Setup(m => m.TryAddMonitor(mockMonitor.Object)).Returns(true);

            var svc = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            svc.RegisterMonitor();

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void RegisterMonitor_ShouldNotDispatchInProgressJobs_WhenAMonitorRegisters() {
            var inProgressJob = new RenderJob { Status = RenderJobStatus.InProgress };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { inProgressJob }.AsQueryable());
            mockMap.Setup(m => m.TryAddMonitor(mockMonitor.Object)).Returns(true);

            var svc = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            svc.RegisterMonitor();

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void RegisterMonitor_ShouldNotDispatchCompleteJobs_WhenAMonitorRegisters() {
            var completeJob = new RenderJob { Status = RenderJobStatus.Complete };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { completeJob }.AsQueryable());
            mockMap.Setup(m => m.TryAddMonitor(mockMonitor.Object)).Returns(true);

            var svc = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            svc.RegisterMonitor();

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        [Description("If we dispatch the entity straight from the database then WCF won't know how to serialize it.")]
        public void RegisterMonitor_ShouldNotDispatchTheExactJobEntity_ToTheNewMonitor() {
            var queuedJob = new RenderJob { Scene = "foo.pbrt", ID = 55, Status = RenderJobStatus.WaitingForDispatch };
            RenderJob actualJobDispatched = null;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.TryAddMonitor(It.IsAny<IRenderMonitor>())).Returns(true);
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMap.Setup(m => m.Monitors).Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { queuedJob }.AsQueryable());
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(queuedJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()))
                       .Callback<RenderJob>(j => actualJobDispatched = j);

            var mgr = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            mgr.RegisterMonitor();

            Assert.AreNotSame(queuedJob, actualJobDispatched);
        }

        [TestMethod]
        public void RegisterMonitor_ShouldUpdateTheJobEntityFromTheDb_WhenDispatchingToNewlyRegisteredMonitor() {
            var existingJobInDb = new RenderJob { Scene = "foo.pbrt", ID = 55, Status = RenderJobStatus.WaitingForDispatch };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.TryAddMonitor(It.IsAny<IRenderMonitor>())).Returns(true);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { existingJobInDb }.AsQueryable());
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), existingJobInDb.ID))
                    .Returns(existingJobInDb);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), existingJobInDb))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));

            var mgr = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            mgr.RegisterMonitor();

            mockRepo.Verify();
        }

        [TestMethod]
        public void RegisterMonitor_ShouldUpdateTheStatusOfTheJobEntityFromTheDb_WhenDispatchingToNewlyRegisteredMonitor() {
            var existingJobInDb = new RenderJob { Scene = "foo.pbrt", ID = 55, Status = RenderJobStatus.WaitingForDispatch };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.TryAddMonitor(It.IsAny<IRenderMonitor>())).Returns(true);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.FindAll(It.IsAny<IUnitOfWork>()))
                    .Returns(new List<RenderJob>() { existingJobInDb }.AsQueryable());
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), existingJobInDb.ID))
                    .Returns(existingJobInDb);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));

            var mgr = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            mgr.RegisterMonitor();

            Assert.AreEqual(RenderJobStatus.InProgress, existingJobInDb.Status);
        }

        [TestMethod]
        public void UnregisterMonitor_ShouldRemoveMonitorFromAssignmentMap() {
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            IEnumerable<int> jobs = new List<int>();
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.TryRemoveMonitor(mockMonitor.Object, out jobs))
                   .Returns(true)
                   .Verifiable();
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            var svc = CreateRenderService(mockOpMgr.Object,
                                          new Mock<IRepository<RenderJob>>(MockBehavior.Strict).Object,
                                          mockMap.Object);

            svc.UnregisterMonitor();

            mockMap.Verify();
        }

        [TestMethod]
        public void UnregisterMonitor_ShouldReassignUnregisteredMonitorJobs() {
            var jobAssignedToUnregisteringMonitor = new RenderJob { Status = RenderJobStatus.InProgress };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            IEnumerable<int> jobs = new List<int>() { jobAssignedToUnregisteringMonitor.ID };
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockMap.Setup(m => m.TryRemoveMonitor(mockMonitor.Object, out jobs))
                   .Callback(() => mockMap.Setup(m => m.Monitors).Returns(new List<IRenderMonitor>()))
                   .Returns(true);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(mockMonitor.Object);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(jobAssignedToUnregisteringMonitor);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForDispatch)))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(mockOpMgr.Object,
                                          mockRepo.Object,
                                          mockMap.Object);

            svc.UnregisterMonitor();

            mockRepo.Verify();
        }

        [TestMethod]
        public void SubmitRender_ShouldAddWaitingForDispatchJob_BeforeJobIsDispatched() {
            var expectedTask = CreateRenderInfo("foo.pbrt");
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>());
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), expectedTask.ID))
                    .Returns<RenderJob>(null);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForDispatch)));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(expectedTask);

            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void SubmitRender_ShouldDispatchJobToRenderMonitor_WhenMonitorIsAvailable() {
            var expectedJob = CreateRenderInfo("job.pbrt");
            RenderJob actualJob = null;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(expectedJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()))
                       .Callback<RenderJob>(j => actualJob = j);

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(expectedJob);

            Assert.AreEqual(expectedJob.ID, actualJob.ID);
        }

        [TestMethod]
        public void SubmitRender_ShallowCopyCodeCoverage() {
            var job = new RenderJob {
                Scene = "foo.lxs",
                Description = new RenderDescription(),
                Results = new List<RenderResult>() {  new RenderResult() }
            };
            RenderJob actualJob = null;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()))
                       .Callback<RenderJob>(j => actualJob = j);

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);
        }

        [TestMethod]
        public void SubmitRender_ShouldDispatchJob_WithInProgressStatus_ToRenderMonitor() {
            var job = CreateRenderInfo("job.pbrt");
            job.Status = RenderJobStatus.WaitingForDispatch;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.Is<RenderJob>(j => j.Status == RenderJobStatus.InProgress)));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void SubmitRender_ShouldAddDispatchedJob_ToAssignmentMap() {
            var job = CreateRenderInfo("job.pbrt");
            job.Status = RenderJobStatus.WaitingForDispatch;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(mockMonitor.Object, job.ID))
                   .Verifiable();
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockMap.Verify();
        }

        [TestMethod]
        public void SubmitRender_ShouldDispatchJob_WithCorrectXResolution_ToRenderMonitor() {
            var expectedXRes = 100;
            var job = new RenderJob {
                ID = 3,
                Status = RenderJobStatus.WaitingForSubmission,
                XResolution = expectedXRes,
                YResolution = 200,
                SamplesPerPixel = 50
            };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.Is<RenderJob>(j => j.XResolution == expectedXRes)));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void SubmitRender_ShouldDispatchJob_WithCorrectYResolution_ToRenderMonitor() {
            var expectedYRes = 200;
            var job = new RenderJob {
                ID = 3,
                Status = RenderJobStatus.WaitingForSubmission,
                XResolution = 100,
                YResolution = expectedYRes,
                SamplesPerPixel = 50
            };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.Is<RenderJob>(j => j.YResolution == expectedYRes)));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var mgr = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            mgr.SubmitRender(job);

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void SubmitRender_ShouldDispatchJob_WithCorrectHaltSpp_ToRenderMonitor() {
            var expectedSpp = 50;
            var job = new RenderJob {
                ID = 3,
                Status = RenderJobStatus.WaitingForSubmission,
                XResolution = 100,
                YResolution = 200,
                SamplesPerPixel = expectedSpp
            };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.Is<RenderJob>(j => j.SamplesPerPixel == expectedSpp)));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void SubmitRender_ShouldDispatchJob_WithCorrectProgressiveSteps_ToRenderMonitor() {
            var expectedProgressiveSteps = 50;
            var job = new RenderJob {
                ID = 3,
                Status = RenderJobStatus.WaitingForSubmission,
                XResolution = 100,
                YResolution = 200,
                SamplesPerPixel = 10,
                ProgressiveSteps = expectedProgressiveSteps
            };
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.Is<RenderJob>(j => j.ProgressiveSteps == expectedProgressiveSteps)));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockMonitor.VerifyAll();
        }

        [TestMethod]
        public void SubmitRender_ShouldKeepJobStateAsNotDispatched_IfDispatchToRenderMonitorFails() {
            var job = CreateRenderInfo("job.pbrt");
            job.Status = RenderJobStatus.WaitingForDispatch;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            IEnumerable<int> jobIds = new List<int>();
            mockMap.Setup(m => m.TryRemoveMonitor(It.IsAny<IRenderMonitor>(), out jobIds))
                   .Callback(() => mockMap.Setup(m => m.Monitors).Returns(new List<IRenderMonitor>()))
                   .Returns(true);
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()))
                       .Throws(new Exception("something went wrong"));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(),
                                         It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForDispatch)))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockRepo.Verify();
        }

        [TestMethod]
        public void SubmitRender_ShouldUpdateExistingJob_InTheRepository() {
            var job = CreateRenderInfo("job.pbrt");
            job.Status = RenderJobStatus.WaitingForDispatch;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), job.ID))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), job)).Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockRepo.Verify();
        }

        [TestMethod]
        public void SubmitRender_ShouldAddNewJob_ToTheRepository() {
            var job = CreateRenderInfo("job.pbrt");
            job.Status = RenderJobStatus.WaitingForDispatch;
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { mockMonitor.Object });
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            mockMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), job.ID))
                    .Returns<RenderJob>(null);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), job))
                    .Callback<IUnitOfWork, RenderJob>((w, j) => {
                        mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), job.ID))
                                .Returns(job);
                    })
                    .Verifiable();
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockRepo.Verify();
        }

        [TestMethod]
        public void ReportRenderResult_ShouldIgnoreResult_IfItComesFromAMonitorThatIsNotInTheAssignmentMap() {
            var result = new RenderResult() { RenderJob = new RenderJob() { Results = new List<RenderResult>() } };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(false);
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(result);

            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void ReportRenderResult_ShouldAddNewRenderJobToRepository() {
            var expectedResult = new RenderResult() { RenderJob = new RenderJob() { Results = new List<RenderResult>() } };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), expectedResult.RenderJob.ID))
                    .Returns<RenderJob>(null);
            mockRepo.Setup(r => r.Add(It.IsAny<IUnitOfWork>(), expectedResult.RenderJob));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(expectedResult);

            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void ReportRenderResult_ShouldUpdateExistingRenderJob() {
            var existingJob = new RenderJob() { Results = new List<RenderResult>() };
            var result = new RenderResult() { RenderJob = existingJob };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), result.RenderJob.ID))
                    .Returns(existingJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), existingJob));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>())).Verifiable();
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(result);

            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void ReportRenderResult_ShouldAddResult_ToRenderJob() {
            var job = new RenderJob() { Status = RenderJobStatus.InProgress, Results = new List<RenderResult>() };
            var result = new RenderResult() { RenderJob = job, Status = RenderResultStatus.Rendering };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), result.RenderJob.ID))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>())).Verifiable();
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(result);

            CollectionAssert.Contains(job.Results.ToList(), result);
        }

        [TestMethod]
        public void ReportRenderResult_ShouldSetResultJob_ToExistingRenderJob() {
            var existingJob = new RenderJob() { Status = RenderJobStatus.InProgress, Results = new List<RenderResult>() };
            var jobWithSameIDAsExistingJob = new RenderJob { ID = existingJob.ID };
            var result = new RenderResult() { RenderJob = jobWithSameIDAsExistingJob, Status = RenderResultStatus.Parsing };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), result.RenderJob.ID))
                    .Returns(existingJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(result);

            Assert.AreEqual(existingJob, result.RenderJob);
        }

        [TestMethod]
        public void ReportRenderResult_ShouldUnassignJobFromMonitor_WhenFinalResultIsReported() {
            var job = new RenderJob { Status = RenderJobStatus.InProgress };
            var result = new RenderResult { RenderJob = job, IsFinalResult = true };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var monitor = new Mock<IRenderMonitor>(MockBehavior.Strict).Object;
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockMap.Setup(m => m.TryGetAssignedMonitor(job.ID, out monitor))
                   .Returns(true);
            mockMap.Setup(m => m.UnassignJobFromMonitor(monitor, job.ID))
                   .Verifiable();
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), result.RenderJob.ID))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(result);

            mockMap.VerifyAll();
        }

        [TestMethod]
        public void ReportRenderResult_ShouldSaveJobAsComplete_WhenFinalResultIsReported() {
            var job = new RenderJob { Status = RenderJobStatus.InProgress };
            var result = new RenderResult { RenderJob = job, IsFinalResult = true };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var monitor = new Mock<IRenderMonitor>(MockBehavior.Strict).Object;
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockMap.Setup(m => m.TryGetAssignedMonitor(job.ID, out monitor))
                   .Returns(true);
            mockMap.Setup(m => m.UnassignJobFromMonitor(monitor, job.ID))
                   .Verifiable();
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), result.RenderJob.ID))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.Is<RenderJob>(j => j.Status == RenderJobStatus.Complete)));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            svc.ReportRenderResult(result);

            mockMap.VerifyAll();
            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void OnRenderJobComplete_ShouldFireWithCompletedJob_WhenFinalResultIsReported() {
            var expectedJob = new RenderJob() { Status = RenderJobStatus.InProgress, Results = new List<RenderResult>() };
            var finalResult = new RenderResult() {
                RenderJob = expectedJob,
                Status = RenderResultStatus.Complete,
                IsFinalResult = true
            };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var monitor = new Mock<IRenderMonitor>(MockBehavior.Strict).Object;
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockMap.Setup(m => m.TryGetAssignedMonitor(It.IsAny<int>(), out monitor))
                   .Returns(true);
            mockMap.Setup(m => m.UnassignJobFromMonitor(monitor, It.IsAny<int>()))
                   .Verifiable();
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), finalResult.RenderJob.ID))
                    .Returns(expectedJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>())).Verifiable();
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            RenderJob actualJob = null;
            svc.OnRenderJobComplete += (s, e) => actualJob = e.RenderJob;

            svc.ReportRenderResult(finalResult);

            Assert.AreEqual(expectedJob, actualJob);
        }

        [TestMethod]
        public void OnRenderJobComplete_ShouldNotFire_WhenNonFinalResultIsReported() {
            var job = new RenderJob() { Status = RenderJobStatus.InProgress, Results = new List<RenderResult>() };
            var nonFinalResult = new RenderResult() {
                RenderJob = job,
                Status = RenderResultStatus.Complete,
                IsFinalResult = false
            };
            var mockOpMgr = new Mock<IOperationContextManager>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockOpMgr.Setup(o => o.GetCallbackChannel<IRenderMonitor>())
                     .Returns(new Mock<IRenderMonitor>(MockBehavior.Strict).Object);
            mockMap.Setup(m => m.ContainsMonitor(It.IsAny<IRenderMonitor>()))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), nonFinalResult.RenderJob.ID))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>())).Verifiable();
            var svc = CreateRenderService(mockOpMgr.Object, mockRepo.Object, mockMap.Object);

            var onRenderJobCompleteEventSent = false;
            svc.OnRenderJobComplete += (s, e) => onRenderJobCompleteEventSent = true;

            svc.ReportRenderResult(nonFinalResult);

            Assert.IsFalse(onRenderJobCompleteEventSent);
        }

        [TestMethod]
        public void GetMonitorInformation_ShouldReturnTheMonitorInformation_OfEveryRegisteredMonitor() {
            var expectedMonitorInfo = new List<RenderMonitorInfo> {
                new RenderMonitorInfo(),
                new RenderMonitorInfo()
            };
            var mockMonitorOne = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockMonitorTwo = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor> {
                       mockMonitorOne.Object,
                       mockMonitorTwo.Object
                   });
            mockMonitorOne.Setup(m => m.GetInformation()).Returns(expectedMonitorInfo[0]);
            mockMonitorTwo.Setup(m => m.GetInformation()).Returns(expectedMonitorInfo[1]);
            var svc = CreateRenderService(assignmentMap: mockMap.Object);

            var actualMonitorInfo = svc.GetMonitorInformation();

            CollectionAssert.AreEqual(expectedMonitorInfo, actualMonitorInfo.ToList());
        }

        [TestMethod]
        public void SubmitRender_ShouldAssignWorkToMonitorWithLeastWork_WhenThereAreMultipleMonitors() {
            var job = new RenderJob { Status = RenderJobStatus.WaitingForDispatch };
            var monitorWithLessWork = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var monitorWithMoreWork = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            IEnumerable<int> moreWork = new List<int>() { 4, 5, 6, 7, 8 };
            mockMap.Setup(m => m.TryGetAssignedJobIds(monitorWithMoreWork.Object, out moreWork))
                   .Returns(true);
            IEnumerable<int> lessWork = new List<int>() { 1, 2, 3 };
            mockMap.Setup(m => m.TryGetAssignedJobIds(monitorWithLessWork.Object, out lessWork))
                   .Returns(true);
            monitorWithLessWork.Setup(m => m.RenderAsync(It.IsAny<RenderJob>()))
                                .Verifiable();
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor> {
                       monitorWithMoreWork.Object,
                       monitorWithLessWork.Object
                   });
            mockMap.Setup(m => m.AssignJobToMonitor(monitorWithLessWork.Object, job.ID))
                   .Verifiable();
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));

            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            monitorWithLessWork.Verify();
            mockMap.Verify();
        }

        [TestMethod]
        public void SubmitRender_ShouldUnregisterFaultedMonitor_WhenDispatchFails() {
            var job = new RenderJob();
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor> { mockMonitor.Object });
            mockMonitor.Setup(m => m.RenderAsync(It.IsAny<RenderJob>()))
                       .Throws(new Exception());
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            IEnumerable<int> jobs = new List<int>();
            mockMap.Setup(m => m.TryRemoveMonitor(mockMonitor.Object, out jobs))
                   .Returns(true)
                   .Verifiable();
            
            var svc = CreateRenderService(repo: mockRepo.Object,
                                          assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockMap.Verify();
        }

        [TestMethod]
        public void Submit_Render_ShouldReassignWorkFromFaultedMonitor_WhenDispatchFails() {
            var job = CreateRenderInfo("job.pbrt");
            job.Status = RenderJobStatus.WaitingForSubmission;
            var faultyMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var goodMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            // The faulty monitor has no work so he gets assigned the incoming job
            IEnumerable<int> faultyMonJobs = new List<int>();
            mockMap.Setup(m => m.TryGetAssignedJobIds(faultyMonitor.Object, out faultyMonJobs))
                   .Returns(true);
            IEnumerable<int> goodMonJobs = new List<int>() { 5, 6, 7 };
            mockMap.Setup(m => m.TryGetAssignedJobIds(goodMonitor.Object, out goodMonJobs))
                   .Returns(true);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { faultyMonitor.Object, goodMonitor.Object });
            IEnumerable<int> jobIds = new List<int>() { job.ID };
            mockMap.Setup(m => m.TryRemoveMonitor(It.IsAny<IRenderMonitor>(), out jobIds))
                   .Returns(true);
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            faultyMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()))
                       // The second time this is called it should return only the good monitor.
                       .Callback(() => mockMap.Setup(map => map.Monitors)
                                              .Returns(new List<IRenderMonitor>() { goodMonitor.Object }))
                       .Throws(new Exception("something went wrong"));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            goodMonitor.Setup(m => m.RenderAsync(It.Is<RenderJob>(j => j.ID == job.ID)))
                       .Verifiable();
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            goodMonitor.Verify();
        }

        [TestMethod]
        public void Submit_Render_ShouldResetRenderJobStatusToWaitingForDispatch_WhenReassigningThemAfterFailedDispatch() {
            var job = new RenderJob();
            job.Status = RenderJobStatus.InProgress;
            var faultyMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var goodMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            // The faulty monitor has no work so he gets assigned the incoming job
            IEnumerable<int> faultyMonJobs = new List<int>();
            mockMap.Setup(m => m.TryGetAssignedJobIds(faultyMonitor.Object, out faultyMonJobs))
                   .Returns(true);
            IEnumerable<int> goodMonJobs = new List<int>() { 5, 6, 7 };
            mockMap.Setup(m => m.TryGetAssignedJobIds(goodMonitor.Object, out goodMonJobs))
                   .Returns(true);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor>() { faultyMonitor.Object, goodMonitor.Object });
            IEnumerable<int> jobIds = new List<int>() { job.ID };
            mockMap.Setup(m => m.TryRemoveMonitor(It.IsAny<IRenderMonitor>(), out jobIds))
                   .Returns(true);
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            faultyMonitor.Setup(c => c.RenderAsync(It.IsAny<RenderJob>()))
                // The second time this is called it should return only the good monitor.
                       .Callback(() => mockMap.Setup(map => map.Monitors)
                                              .Returns(new List<IRenderMonitor>() { goodMonitor.Object }))
                       .Throws(new Exception("something went wrong"));
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(job);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()))
                    // The second time we update will be when we reassign the job...
                    .Callback(() => {
                        mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(),
                                                     It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForDispatch)))
                                // and the last time is after the reassigned job succeeds
                                .Callback(() => mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(),
                                                                             It.IsAny<RenderJob>())));
                    });
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            goodMonitor.Setup(m => m.RenderAsync(It.IsAny<RenderJob>()))
                       .Verifiable();
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            svc.SubmitRender(job);

            mockRepo.VerifyAll();
        }

        [TestMethod]
        public void GetMonitorInformation_ShouldReturnInfoFromAllGoodMonitors_EvenIfThereAreSomeFaultyMonitors() {
            var expectedInfo = new List<RenderMonitorInfo> { new RenderMonitorInfo() };
            var jobInFaultyMonitor = new RenderJob { Status = RenderJobStatus.InProgress };
            var faultyMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var goodMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor> {
                       faultyMonitor.Object,
                       goodMonitor.Object
                   });
            faultyMonitor.Setup(m => m.GetInformation())
                         .Throws(new Exception());
            goodMonitor.Setup(m => m.GetInformation()).Returns(expectedInfo[0]);
            IEnumerable<int> jobIds = new List<int>() { jobInFaultyMonitor.ID };
            mockMap.Setup(m => m.TryRemoveMonitor(It.IsAny<IRenderMonitor>(), out jobIds))
                   .Callback(() => mockMap.Setup(m => m.Monitors)
                                          .Returns(new List<IRenderMonitor>() { goodMonitor.Object }))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(jobInFaultyMonitor);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMap.Setup(m => m.AssignJobToMonitor(It.IsAny<IRenderMonitor>(), It.IsAny<int>()));
            goodMonitor.Setup(m => m.RenderAsync(It.IsAny<RenderJob>()));
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            var actualInfo = svc.GetMonitorInformation();

            CollectionAssert.AreEquivalent(expectedInfo, actualInfo.ToList());
        }

        [TestMethod]
        public void GetMonitorInformation_ShouldReassignWorkFromFaultedMonitor_AfterGetMonitorInformationFails() {
            var jobInFaultyMonitor = new RenderJob { Status = RenderJobStatus.InProgress };
            var faultyMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var goodMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Returns(new List<IRenderMonitor> {
                       faultyMonitor.Object,
                       goodMonitor.Object
                   });
            faultyMonitor.Setup(m => m.GetInformation())
                         .Throws(new Exception());
            goodMonitor.Setup(m => m.GetInformation()).Returns(new RenderMonitorInfo());
            IEnumerable<int> jobIds = new List<int>() { jobInFaultyMonitor.ID };
            mockMap.Setup(m => m.TryRemoveMonitor(It.IsAny<IRenderMonitor>(), out jobIds))
                   .Callback(() => mockMap.Setup(m => m.Monitors)
                                          .Returns(new List<IRenderMonitor>() {  goodMonitor.Object }))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(jobInFaultyMonitor);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            mockMap.Setup(m => m.AssignJobToMonitor(goodMonitor.Object, jobInFaultyMonitor.ID))
                   .Verifiable();
            goodMonitor.Setup(m => m.RenderAsync(It.Is<RenderJob>(j => j.ID == jobInFaultyMonitor.ID)))
                       .Verifiable();
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            // make sure to evaluate the response...
            var info = svc.GetMonitorInformation().ToList();

            goodMonitor.Verify();
            mockMap.Verify();
        }

        [TestMethod]
        public void GetMonitorInformation_ShouldResetRenderJobStatusToWaitingForDispatch_WhenReassigning() {
            var jobInFaultyMonitor = new RenderJob { Status = RenderJobStatus.InProgress };
            var faultyMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockMap.Setup(m => m.Monitors)
                   .Callback(() => mockMap.Setup(m => m.Monitors).Returns(new List<IRenderMonitor>()))
                   .Returns(new List<IRenderMonitor> {
                       faultyMonitor.Object
                   });
            faultyMonitor.Setup(m => m.GetInformation())
                         .Throws(new Exception());
            IEnumerable<int> jobIds = new List<int>() { jobInFaultyMonitor.ID };
            mockMap.Setup(m => m.TryRemoveMonitor(It.IsAny<IRenderMonitor>(), out jobIds))
                   .Returns(true);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(jobInFaultyMonitor);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForDispatch)))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            // make sure to evaluate the response...
            var info = svc.GetMonitorInformation().ToList();

            mockRepo.Verify();
        }

        [TestMethod]
        public void StopRender_ShouldDoNothing_IfJobDoesNotExistInTheRepo() {
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns<RenderJob>(null);
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(20);

            // Just making sure that nothing else happens...
        }

        [TestMethod]
        public void StopRender_ShouldDoNothing_IfRenderIsAlreadyStopped() {
            var stoppedJob = new RenderJob { Status = RenderJobStatus.WaitingForSubmission };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(stoppedJob);
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(20);

            // Just making sure that nothing else happens...
        }

        [TestMethod]
        public void StopRender_ShouldDoNothing_IfRenderIsAlreadyComplete() {
            var completeJob = new RenderJob { Status = RenderJobStatus.Complete };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(completeJob);
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(20);

            // Just making sure that nothing else happens...
        }

        [TestMethod]
        public void StopRender_ShouldChangeJobStatus_ToWaitingForSubmission_IfJobIsWaitingForDispatch() {
            var waitingJob = new RenderJob { Status = RenderJobStatus.WaitingForDispatch };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(waitingJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(20);

            Assert.AreEqual(RenderJobStatus.WaitingForSubmission, waitingJob.Status);
        }

        [TestMethod]
        public void StopRender_ShouldUpdateCorrectJobInRepo() {
            var waitingJob = new RenderJob { Status = RenderJobStatus.WaitingForDispatch };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(waitingJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), waitingJob))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(waitingJob.ID);

            mockRepo.Verify();
        }

        [TestMethod]
        public void StopRender_ShouldUpdateJobInRepo_WithNewJobStatus() {
            var waitingJob = new RenderJob { Status = RenderJobStatus.WaitingForDispatch };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(waitingJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(),
                                         It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForSubmission)))
                    .Verifiable();
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(waitingJob.ID);

            mockRepo.Verify();
        }

        [TestMethod]
        public void StopRender_ShouldSaveAfterUpdatingJob() {
            var waitingJob = new RenderJob { Status = RenderJobStatus.WaitingForDispatch };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(waitingJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(), It.IsAny<RenderJob>()));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()))
                    .Verifiable();
            var svc = CreateRenderService(repo: mockRepo.Object);

            svc.StopRender(waitingJob.ID);

            mockRepo.Verify();
        }

        [TestMethod]
        public void StopRender_ShouldCallStopRender_InAppropriateMonitor_WhenStoppingInProgressJob() {
            var inProgressJob = new RenderJob { Status = RenderJobStatus.InProgress };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(inProgressJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(),
                                         It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForSubmission)));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var assignedMonitor = mockMonitor.Object;
            mockMap.Setup(m => m.TryGetAssignedMonitor(It.IsAny<int>(), out assignedMonitor)).Returns(true);
            mockMonitor.Setup(m => m.StopRender(It.Is<RenderJob>(p => p.ID == inProgressJob.ID)))
                       .Verifiable();
            mockMap.Setup(m => m.UnassignJobFromMonitor(assignedMonitor, inProgressJob.ID));
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            svc.StopRender(inProgressJob.ID);

            mockMonitor.Verify();
        }

        [TestMethod]
        public void StopRender_ShouldCallRemoveInProgressJobFromAssignmentMap_AfterMonitorHasStoppedIt() {
            var inProgressJob = new RenderJob { Status = RenderJobStatus.InProgress };
            var mockRepo = new Mock<IRepository<RenderJob>>(MockBehavior.Strict);
            var mockMap = new Mock<IRenderJobAssignmentMap>(MockBehavior.Strict);
            var mockMonitor = new Mock<IRenderMonitor>(MockBehavior.Strict);
            mockRepo.Setup(r => r.Get(It.IsAny<IUnitOfWork>(), It.IsAny<long>()))
                    .Returns(inProgressJob);
            mockRepo.Setup(r => r.Update(It.IsAny<IUnitOfWork>(),
                                         It.Is<RenderJob>(j => j.Status == RenderJobStatus.WaitingForSubmission)));
            mockRepo.Setup(r => r.Save(It.IsAny<IUnitOfWork>()));
            var assignedMonitor = mockMonitor.Object;
            mockMap.Setup(m => m.TryGetAssignedMonitor(It.IsAny<int>(), out assignedMonitor)).Returns(true);
            mockMonitor.Setup(m => m.StopRender(It.IsAny<RenderJob>()));
            mockMap.Setup(m => m.UnassignJobFromMonitor(assignedMonitor, inProgressJob.ID))
                   .Verifiable();
            var svc = CreateRenderService(repo: mockRepo.Object, assignmentMap: mockMap.Object);

            svc.StopRender(inProgressJob.ID);

            mockMonitor.Verify();
        }
    }
}